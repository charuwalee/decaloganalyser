package myUtils;

public class Triplet {
	double	x, y, z;
	
	public Triplet () {
		x = 0.;
		y = 0.;
		z = 0.;
	}
	
	public Triplet (Triplet org) {
		this.x = org.x;
		this.y = org.y;
		this.z = org.z;
	}
	
	public Triplet (double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Triplet (double x, double y) {
		this.x = x;
		this.y = y;
		this.z = 0;
	}
	
	public Triplet (double[] pos) {
		setValues(pos);
	}
	
	public Triplet (String[] posstr) {
		setValues(posstr);
	}
	
	@Override
	public Triplet clone () {
		return new Triplet(this.x, this.y, this.z);
	}
	
	public String print () {
		return "(" + x + ", " + y + ", " + z + ")";
	}
	
	public void setValues (double[] pos) {
		if (pos.length >= 3) {
			this.z = pos[2];
		}
		if (pos.length >= 2) {
			this.y = pos[1];
		}
		if (pos.length >= 1) {
			this.x = pos[0];
		}
	}
	
	public void setValues (double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public void setValues (double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void setValues (String[] valstr) {
		if (valstr.length >= 3) {
			this.z = Double.parseDouble(valstr[2]);
		}
		if (valstr.length >= 2) {
			this.y = Double.parseDouble(valstr[1]);
		}
		if (valstr.length >= 1) {
			this.x = Double.parseDouble(valstr[0]);
		}
	}
	
	public void setValue (String pos, double val) {
		if (pos.equalsIgnoreCase("x")) {
			this.x = val;
		}
		else if (pos.equalsIgnoreCase("y")) {
			this.y = val;
		}
		else if (pos.equalsIgnoreCase("z")) {
			this.z = val;
		}
	}
	
	public void setValue (int i, double val) {
		if (i==0) {
			this.x = val;
		}
		else if (i==1) {
			this.y = val;
		}
		else if (i==2) {
			this.z = val;
		}
	}
	
	public double getX () {
		return this.x;
	}
	
	public double getY () {
		return this.y;
	}
	
	public double getZ () {
		return this.z;
	}
	
	public String toString () {
		return "(" + x + ", " + y + ", " + z + ")";
	}
	
	public String printCSV (Dimension d) {
		return (d == Dimension.THREED) ? x + "," + y + "," + z + "," : x + "," + y + ",";
	}
	
	public double rangeWith (Triplet t) {
		return Math.sqrt(Math.pow(t.getX()-x, 2) + Math.pow(t.getY()-y, 2) + Math.pow(t.getZ()-z, 2));
	}
	
	public double rangeWith (double xx, double yy, double zz) {
		return Math.sqrt(Math.pow(xx-x, 2) + Math.pow(yy-y, 2) + Math.pow(zz-z, 2));
	}
	
	public double rangeWith (double xx, double yy) {
		return Math.sqrt(Math.pow(xx-x, 2) + Math.pow(yy-y, 2));
	}
	
	public double sumOfSquared (Dimension dimension) {
		double sum = Math.pow(x, 2)+Math.pow(y, 2);;
		
		if (dimension == Dimension.THREED) {
			sum += Math.pow(z, 2);
		}
		
		return sum;
	}
	
	public Triplet minus (Triplet t) {
		return new Triplet (x-t.getX(), y-t.getY(), z-t.getZ());
	}
	
	public Triplet plus (Triplet t) {
		return new Triplet (x+t.getX(), y+t.getY(), z+t.getZ());
	}
}
