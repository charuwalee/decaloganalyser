package decaloganalyser;

import java.io.*;
import java.util.*;

import myUtils.*;


public class LogTDOAProcessor {

	//global
	static final double		SPEED_OF_LIGHT_MM					= 299702547000.0;			//in millimeter
	static final double		CLOCK_PERIOD_S						= 0.00000000001565004;		//in second
	static final String 	CSV_FILE_CONTENTS_DELIMETER			= ",";
	static final String		CONSOLE_DISPLAY_SECTION				= "=========================";
	static final int		HISTOGRAM_FIRST_STAGE				= 10;
	static final int		HISTOGRAM_STEP						= 10;
	static final int 		MAX_SAMPLE_NUMBER					= 2000;
	static final int		ORIGINATED_ANC_NUMBER				= 4;
	static final String		ISO_ANALYSED_FILE_SUFFIX			= "_ISO_analysed";
	static final String		CHAN_HO_ANALYSED_FILE_SUFFIX		= "_CH_analysed";
	static final String		CHAN_HO_EXT_ANALYSED_FILE_SUFFIX	= "_CHEXT_analysed";
	static final String		WIKI_ANALYSED_FILE_SUFFIX			= "_Wiki_analysed";
	
	int linecount;
		
	void parseAndGenTagTDOAReport (ArrayList<String> params) throws Exception {
		// "-algo <algo1> <algo2> ... -file <file1> <file2> ...
		
		String[] paramarray = new String[params.size()];
		paramarray = params.toArray(paramarray);
		
		if (!paramarray[0].equalsIgnoreCase("-algo")) {
			System.out.println("Invalid command parameter. Algorithm parameter is expected.");
			return;
		}
		
		int i=1;
		while (i<params.size() && !paramarray[i].equalsIgnoreCase("-file")) {
			i++;
		}
		
		List<String> algolist = params.subList(1, i);
		
		for (i++; i<params.size(); i++) {
			genTagTDOAReport(algolist, params.get(i));
		}
		
		System.out.println("\nDone reading all files!!!");
		System.out.println(CONSOLE_DISPLAY_SECTION);
	}
	
	
	void genTagTDOAReport (List<String> algolist, String infilename) throws Exception {
		String 	outprefixfilename = infilename.substring(0, infilename.lastIndexOf("."));
		ArrayList<Triplet>  groundtruth = readGroundTruth(ORIGINATED_ANC_NUMBER, infilename);
		RangeDiffTable rangedifftable = readTimestamp(ORIGINATED_ANC_NUMBER, MAX_SAMPLE_NUMBER, infilename); //ancID_ancID -> ArrayList of range_difference (in millimeter)
		
		System.out.println(CONSOLE_DISPLAY_SECTION);
		System.out.println("Reading file "+ infilename.substring(infilename.lastIndexOf("/")+1));
		
		//Simulate range from 5th anchor
		Triplet tag = groundtruth.get(groundtruth.size()-1);
		Triplet newanc = new Triplet(3500., 2000., 2000.);	//in millimeters
		int	newancindex = groundtruth.size()-1;
		//groundtruth.add(newancindex, newanc); to be added when computing traditional Chan-Ho methods
		for (int i=0; i<groundtruth.size(); i++) {
			Triplet oldanc = groundtruth.get(i);
			String str1 = i+"_"+newancindex;
			String str2 = newancindex+"_"+i;
			double val1 = tag.rangeWith(oldanc)-tag.rangeWith(newanc);
			double val2 = -val1;
			for (int n=1; n<=linecount; n++) {
				rangedifftable.put(n, str1, val1);
				rangedifftable.put(n, str2, val2);
			}
		}
		
		
		for (int i=0; i<algolist.size(); i++) {
			String algo = algolist.get(i);
			if (algo.toLowerCase().contains("chanhoext")) {
				System.out.println("\t...compute extended Chan-Ho-based estimates.");
				genCHExtOut(outprefixfilename+CHAN_HO_EXT_ANALYSED_FILE_SUFFIX, groundtruth, rangedifftable);
				continue;
			}
			if (algo.toLowerCase().contains("chanho")) {
				System.out.println("\t...compute original Chan-Ho-based estimates.");
				groundtruth.add(newancindex, newanc);
				genCHOut(outprefixfilename+CHAN_HO_ANALYSED_FILE_SUFFIX, groundtruth, rangedifftable);
				groundtruth.remove(newancindex);
				continue;
			}
			if (algo.toLowerCase().contains("iso")) {
				System.out.println("\t...compute ISO-based estimates.");
				genISOOut(outprefixfilename+ISO_ANALYSED_FILE_SUFFIX, groundtruth, rangedifftable);
				continue;
			}
			if (algo.toLowerCase().contains("wiki")) {
				System.out.println("\t...compute Wiki-based estimates.");
				groundtruth.add(newancindex, newanc);
				genWikiOut(outprefixfilename+WIKI_ANALYSED_FILE_SUFFIX, groundtruth, rangedifftable);
				groundtruth.remove(newancindex);
			}
		}		
	}
			
	
	ArrayList<Triplet> readGroundTruth (int oriancnum, String infilename) throws Exception {
		//Get ground truth first
		//Format: (in millimeter)
		//	Line 1: 		anc0x	,	anc0y	,	anc0z
		//	Line 2: 		anc1x	, 	anc1y	, 	anc1z
		//	
		//	Line ANC_NUM:   ancnx	, 	ancny	, 	ancnz
		// 	Line ANC_NUM+1: tagx	, 	tagy	, 	tagz
		BufferedReader in;
		String 	readline;
		String[] token;
		ArrayList<Triplet>  groundtruth = new ArrayList<Triplet>();
		
		in  = new BufferedReader(new FileReader(new File(infilename)));
		
		for (int i=0; i<=oriancnum; i++) {
			readline = in.readLine();
			token = readline.split(CSV_FILE_CONTENTS_DELIMETER);
			groundtruth.add(new Triplet(token));
		}
		in.close();
		
		return groundtruth;
	}
	
	
	RangeDiffTable readTimestamp (int ancnum, int maxlineread, String infilename) throws Exception {	
		//Start reading timestamp log file (the contents include timestamps from all anchors)
		//[<poll_i>,<rxtimestamp_i>]+
		
		String[] 	token;
		String 		readline, rangediffoutfilename;
		RangeDiffTable  rangedifftable = new RangeDiffTable("RangeDiff");	// ancID_ancID -> ArrayList of range_difference
		
		BufferedReader in = new BufferedReader(new FileReader(new File(infilename)));
		Hashtable<Integer, Long>  rxtslist	 = new Hashtable<Integer, Long>();	//ancID -> rxts	(timestamp value read from one line)
		linecount=0;
		
		//skip a few first lines (info of anchors and a tag)
		for (int i=0; i<=ancnum; i++) {
			readline = in.readLine();
		}
		
		//Read at most MAX_SAMPLE_NUMBER timestamps
		while ((readline = in.readLine()) != null  &&  linecount < maxlineread) {
			rxtslist.clear();
			linecount++;
			
			//check poll values
			token = readline.split(CSV_FILE_CONTENTS_DELIMETER);
			if (!token[0].contentEquals(token[2]) ||
				!token[0].contentEquals(token[4]) ||
				!token[0].contentEquals(token[6])) {
				System.out.println("ReadTimestamp: Mismatch poll ID found at line " + linecount + ". The program terminates.");
				System.exit(0);
			}
			
			//get rxtimestamp
			try {
				rxtslist.put(0, Long.parseUnsignedLong(token[1], 16)); //rxtimestamp from anchor0
				rxtslist.put(1, Long.parseUnsignedLong(token[3], 16)); //rxtimestamp from anchor1
				rxtslist.put(2, Long.parseUnsignedLong(token[5], 16)); //rxtimestamp from anchor2
				rxtslist.put(3, Long.parseUnsignedLong(token[7], 16)); //rxtimestamp from anchor3
			}
			catch (Exception e) {
				System.out.println(this.getClass() + ": " + "readTimeStamp(): Exception " + e.getClass() + " found at sample " + linecount + ".");
				System.exit(0);
			}
			//compute range diff variables
			for (int i=0; i<ancnum; i++) {
				for (int j=0; j<ancnum; j++) {
					if (i!=j) {
						rangedifftable.put(linecount, i+"_"+j, SPEED_OF_LIGHT_MM * CLOCK_PERIOD_S * (rxtslist.get(i) - rxtslist.get(j)));	//range diff in mm
					}
				}
			}
		}//while(readline)
		
		in.close();
		
		rangediffoutfilename = infilename.substring(0, infilename.lastIndexOf(".")) + "_range_diff.csv";
		PrintWriter out = new PrintWriter(new File(rangediffoutfilename));
		
		//print headers and data
		for (int n=0; n<=linecount; n++) {
			String str = "";
			for (int i=0; i<ancnum; i++) {
				for (int j=0; j<ancnum; j++) {
					if (i!=j) {
						str += rangedifftable.printCSV (n, i + "_" + j);
					}
				}
			}
			
			out.println(str);
		}
		
		out.flush();
		out.close();
		return rangedifftable;
	}
	
		
	
	// 1. choose from minimal (2D/3D) range_diff_err of related anchors in the same line
	Triplet chooseByRelatedAnchors (int ancnum, Hashtable<String, Triplet> estsubtable, Hashtable<String, Double> rangedifferrsubtable, TDOA.Variant algorithm) {
		// - estsubtable contains valid estimates from the same line.
		// - rangedifferrsubtable contains either 2D or 3D range difference errors of the same line.
		// Case ISO:
		//  	choose estimate_ijk which gives the minimum range_diff_err_ii_jj where ii and jj are members of {i, j, k}
		// Case Chan-Ho:
		//  	choose estimate_i which gives the mimimum range_diff_err_ii_jj where ii or jj equals i
		// Case Wiki:
		//  	choose estimate_ij which gives the minimum range_diff_err_ii_jj where ii or jj  equals i
		
		double minrangedifferr = Double.MAX_VALUE;
		Triplet tp = null;
		
		if (estsubtable.size() == 1) { 
			tp = estsubtable.elements().nextElement();
		}
		else {
			for (String estindex : estsubtable.keySet()) {
				String ancindex;
				double sumrangedifferrsqr = 0.;
				int	count=0;	
				
				switch (algorithm) {
					case ISO:
						//choose from a set of estimate_ijk where both ii and jj are members of {i, j, k}
						//<estindex> ::= <ancID>_<ancID>_<ancID>_[0|1]
						ancindex = estindex.substring(0, estindex.lastIndexOf("_"));
						for (int ii=0; ii<ancnum-1; ii++) {
							if (ancindex.contains(Integer.toString(ii))) {
								for (int jj=ii+1; jj<ancnum; jj++) {
									if (ancindex.contains(Integer.toString(jj))) {
										String rindex = estindex + "_"+ii+"_"+jj;
										sumrangedifferrsqr += Math.pow(rangedifferrsubtable.get(rindex),2);
										count++;
									}
								}//for (int jj)
							}
						}//for (int ii)
						break;
					
					case CHANHO:
						//choose from a set of estimate_i where ii or jj equals i
						//<estindex> ::= <ancID>
						ancindex = estindex;
						for (int ii=0; ii<ancnum-1; ii++) {
							for (int jj=ii+1; jj<ancnum; jj++) {
								if (ancindex.contains(Integer.toString(ii)) || ancindex.contains(Integer.toString(jj))) {
									String rindex = ancindex+"_"+ii+"_"+jj;
									sumrangedifferrsqr += Math.pow(rangedifferrsubtable.get(rindex),2);
									count++;
								}
							}//for(int jj)
						}//for(int ii)
						break;
					case WIKI:
						//choose from a set of estimate_ij where ii or jj equals i
						//<estindex> ::= <ancID_i>_<ancID_j>
						//ancindex = estindex;
						ancindex = estindex.substring(0, estindex.lastIndexOf("_"));
						for (int ii=0; ii<ancnum-1; ii++) {
							for (int jj=ii+1; jj<ancnum; jj++) {
								if (ancindex.contains(Integer.toString(ii)) || ancindex.contains(Integer.toString(jj))) {
									String rindex = estindex+"_"+ii+"_"+jj;
									sumrangedifferrsqr += Math.pow(rangedifferrsubtable.get(rindex), 2);
									count++;
								}
							}//for(int jj)
						}//for(int ii)
						break;
					default:
						System.out.print(this.getClass() + ": chooseByRelatedAnchors(): Invalid algorithm parameter: " + algorithm);
				}//switch(algorithm)
				
				double temp = Math.sqrt(sumrangedifferrsqr/count);
				if(minrangedifferr > temp) {
					minrangedifferr = temp;
					tp = estsubtable.get(estindex);
				}
			}//for(estindex)
		}
		
		return tp;
	}
	
	
	// 2. choose from minimal range_diff_err of non-related anchors in the same line
	Triplet chooseByNonRelatedAnchors (int ancnum, Hashtable<String, Triplet> estsubtable, Hashtable<String, Double> rangedifferrsubtable, TDOA.Variant algorithm) {
		//estsubtable contains valid estimates from the same linenumber.
		//Case ISO:
		//	choose estimate_ijk which gives the minimum range_diff_err_ii_jj where ii or jj is NOT a member of {i, j, k}
		//Case Chan-Ho:
		//	choose estimate_i which gives the mimimum range_diff_err_ii_jj where ii and jj do NOT equal i
		//Case Wiki:
		//	choose estimate_ij which gives the minimum range_diff_err_ii_jj where ii and jj do NOT equal i

		double minrangedifferr = Double.MAX_VALUE;
		Triplet tp= null;
		
		if (estsubtable.size() == 1) {
			tp = estsubtable.elements().nextElement();
		}
		else {
			for (String estindex : estsubtable.keySet()) {
				String ancindex;
				double sumrangedifferrsqr = 0.;
				int	count=0;	
				
				switch (algorithm) {
					case ISO:
						//choose from a set of estimate_ijk where ii or jj is NOT a member of {i, j, k}
						//<estindex> ::= <ancID>_<ancID>_<ancID>_[0|1]
						ancindex = estindex.substring(0, estindex.lastIndexOf("_"));
						for(int ii=0; (ii<ancnum-1); ii++) {
							for (int jj=ii+1; jj<ancnum; jj++) {
								if (!ancindex.contains(Integer.toString(ii)) || !ancindex.contains(Integer.toString(jj))) {
									String rindex = estindex + "_"+ii+"_"+jj;
									sumrangedifferrsqr += Math.pow(rangedifferrsubtable.get(rindex),2);
									count++;
								}
							}//for (int jj)
						}//for (int ii)
						break;
					
					case CHANHO:
						//choose from a set of estimate_i where ii and jj do NOT equal i
						//<estindex> ::= <ancID>
						ancindex = estindex;
						for (int ii=0; ii<ancnum-1; ii++) {
							for (int jj=ii+1; jj<ancnum; jj++) {
								if (!ancindex.contains(Integer.toString(ii)) && !ancindex.contains(Integer.toString(jj))) {
									String rindex = ancindex+"_"+ii+"_"+jj;
									sumrangedifferrsqr += Math.pow(rangedifferrsubtable.get(rindex),2);
									count++;
								}
							}//for(int jj)
						}//for(int ii)
						break;
					case WIKI:
						//choose from a set of estimate_i where ii and jj do NOT equal i
						//<estindex> ::= <ancID_i>_<ancID_j>
						ancindex = estindex.substring(0, estindex.lastIndexOf("_"));
						for (int ii=0; ii<ancnum-1; ii++) {
							for (int jj=ii+1; jj<ancnum; jj++) {
								if (!ancindex.contains(Integer.toString(ii)) && !ancindex.contains(Integer.toString(jj))) {
									String rindex = estindex+"_"+ii+"_"+jj;
									sumrangedifferrsqr += Math.pow(rangedifferrsubtable.get(rindex), 2);
									count++;
								}
							}//for(int jj)
						}//for(int ii)
						break;
					default:
						System.out.print(this.getClass() + ": chooseByNonRelatedAnchors(): Invalid algorithm parameter: " + algorithm);
				}//switch(algorithm)
				
				double temp = Math.sqrt(sumrangedifferrsqr/count);
				if(minrangedifferr > temp) {
					minrangedifferr = temp;
					tp = estsubtable.get(estindex);
				}
			}//for(estindex)
		}
		
		return tp;
	}
	
	
	// 3. choose from minimal range_diff_err of anchors in the same line
	Triplet chooseByAllAnchors (int ancnum, Hashtable<String, Triplet> estsubtable, Hashtable<String, Double> rangedifferrsubtable) {
		//estsubtable contains valid estimates from the same linenumber.
		
		double minrangedifferr = Double.MAX_VALUE;
		Triplet tp= null;
		
		if (estsubtable.size() == 1) {
			tp = estsubtable.elements().nextElement();
		}
		else {
			for (String estindex : estsubtable.keySet()) {
				double sumrangedifferrsqr = 0.;
				int	count=0;	
				
				for(int ii=0; (ii<ancnum-1); ii++) {
					for (int jj=ii+1; jj<ancnum; jj++) {
						String rindex = estindex + "_"+ii+"_"+jj;
						sumrangedifferrsqr += Math.pow(rangedifferrsubtable.get(rindex),2);
						count++;
					}//for (int jj)
				}//for (int ii)
				
				double temp = Math.sqrt(sumrangedifferrsqr/count);
				if(minrangedifferr > temp) {
					minrangedifferr = temp;
					tp = estsubtable.get(estindex);
				}
			}//for(estindex)
		}
		
		return tp;
	}
	
	
	
	void genISOOut (String outfilename, ArrayList<Triplet> groundtruth, RangeDiffTable rangedifftable) throws Exception {
		EstimateTable 	esttable = new EstimateTable(TDOA.toString(TDOA.Variant.ISO) + "_Estimate");	//line -> "ancID_ancID_ancID_[0|1]" -> Triplet
		RangeDiffTable  rangedifferrtable = new RangeDiffTable(TDOA.toString(TDOA.Variant.ISO) + "_Range_Diff_Error"); //line -> "ancID_ancID_ancID_[0|1]_ancID_ancID" -> Double
		ResultTable 	result_ra = new ResultTable(TDOA.toString(TDOA.Variant.ISO_RA) + "_Result");
		ResultTable 	result_nra = new ResultTable(TDOA.toString(TDOA.Variant.ISO_NRA) + "_Result");
		ResultTable 	result_aa = new ResultTable(TDOA.toString(TDOA.Variant.ISO_AA) + "_Result");
		EuclideanErrorTable err_ra = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.ISO_RA) + "_Error");
		EuclideanErrorTable err_nra = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.ISO_NRA) + "_Error");
		EuclideanErrorTable err_aa = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.ISO_AA) + "_Error");
		
		Triplet tag = groundtruth.get(groundtruth.size()-1);
		int ancnum  = groundtruth.size()-1;
		
		for (int n=1; n<=linecount; n++) {
			//compute valid estimates for data of one line
			for (int i=0; i<ancnum; i++) {
				Triplet anci = groundtruth.get(i);
			  	for (int j=0  ; j<ancnum; j++) {
					if (i != j) {
						Triplet ancj = groundtruth.get(j);
						for (int k=0  ; k<ancnum; k++) {
							if (i != k  &&  j != k) {
								Triplet anck = groundtruth.get(k);
								ISOEstimates estimates = TDOA.computeISOEstimates (rangedifftable.get(n, i+"_"+j), rangedifftable.get(n, i+"_"+k), rangedifftable.get(n, j+"_"+k), anci, ancj, anck);
								esttable.putISOEstimates(n, i+"_"+j+"_"+k, estimates); 
								
								if (estimates.getNumber() >= 1) {
									//compute rangedifferr
									for (int nn=0; nn<estimates.getNumber(); nn++) {
										Triplet est = estimates.getTriplet(nn);
										double estx = est.getX();
										double esty = est.getY();
										for (int ii=0; ii<ancnum-1; ii++) {
											double xii = groundtruth.get(ii).getX();
											double yii = groundtruth.get(ii).getY();
											for (int jj=ii+1; jj<ancnum; jj++) {
												double xjj = groundtruth.get(jj).getX();
												double yjj = groundtruth.get(jj).getY();
												double rangediff_nn_ii_jj = Math.sqrt(Math.pow(estx-xii,2) + Math.pow(esty-yii, 2)) - Math.sqrt(Math.pow(estx-xjj,2) + Math.pow(esty-yjj, 2));
												double colrangediff_ii_jj = rangedifftable.get(n, ii+"_"+jj);
												double rangedifferr = Math.abs(colrangediff_ii_jj - rangediff_nn_ii_jj);
												rangedifferrtable.put(n, i+"_"+j+"_"+k+"_"+nn+"_"+ii+"_"+jj, rangedifferr);
											}//for (int jj)
										}//for (int ii)
									}//for (int nn)
								}//if (estimates.getNumber() >= 1)
							}//if (i!=k && j!=k)
						}//for (int k)
					}//if (i != j)
				}//for (int j)
			}//for (int i)
			
			
			Triplet tp;
			if (esttable.containsKey(n) && (tp=chooseByRelatedAnchors(ancnum, esttable.get(n), rangedifferrtable.get(n), TDOA.Variant.ISO)) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY()); 
				result_ra.put(n, tp);
				err_ra.put(n, err);
				//err_iso_ra.add(err);
			}
			if (esttable.containsKey(n) && (tp=chooseByNonRelatedAnchors(ancnum, esttable.get(n), rangedifferrtable.get(n), TDOA.Variant.ISO)) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_nra.put(n, tp);
				err_nra.put(n, err);
				//err_iso_nra.add(err);
			}
			if (esttable.containsKey(n) && (tp=chooseByAllAnchors(ancnum, esttable.get(n), rangedifferrtable.get(n))) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_aa.put(n, tp);
				err_aa.put(n, err);
				//err_iso_aa.add(err);
			}			
		}//for(int n)
		
				
		//print headers and results
		PrintWriter isoout = new PrintWriter(new File(outfilename+".csv"));
		for (int n=0; n<=linecount; n++) {
			String str = "";
			for (int i=0; i<ancnum; i++) {
			 	for (int j=0; j<ancnum; j++) {
					if (i != j) {
					  	for (int k=0; k<ancnum; k++) {
							if (i != k  &&  j != k) {
								String relatedancindex = i+"_"+j+"_"+k;
								
								//x0est, y0est,x1est, y1est,
								str += esttable.printCSV(n, relatedancindex + "_0", Dimension.TWOD)
									 + esttable.printCSV(n, relatedancindex + "_1", Dimension.TWOD);
								
								//[rangedifferr_i_j_k_0_ii_jj, rangedifferr_i_j_k_1_ii_jj]+
								for (int ii=0; ii<ancnum-1; ii++) {
									for (int jj=ii+1; jj<ancnum; jj++) {
										str += rangedifferrtable.printCSV(n, relatedancindex+ "_0_" + ii + "_" + jj);
										str += rangedifferrtable.printCSV(n, relatedancindex+ "_1_" + ii + "_" + jj);
									}//for (int jj)
								}//for (int ii)
							}//if (i!=k && j!=k)	
						}// for (int k)
					}//if (i!=j)
				}// for (int j)
			}// for (int i)
			
			
			//"x_RA,y_RA,x_NRA,y_NRA,x_AA,y_AA, "
			str += result_ra.printCSV(n, Dimension.TWOD);
			str += result_nra.printCSV(n, Dimension.TWOD);
			str += result_aa.printCSV(n, Dimension.TWOD);
			
			//"range_error_RA, range_error2_NRA, range_error_AA,"
			str += err_ra.printCSV(n);
			str += err_nra.printCSV(n);
			str += err_aa.printCSV(n);
						
			isoout.println(str);
		}//for (int n)
		
		isoout.flush();
		isoout.close();
	}
	
		
	
	void genCHOut (String outfilename, ArrayList<Triplet> groundtruth, RangeDiffTable rangedifftable) throws Exception {
		EstimateTable 	est_table_ols_2d = new EstimateTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_2D) + "_Estimate");	//line -> "ancID" -> Triplet
		EstimateTable 	est_table_ols_3d = new EstimateTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_3D) + "_Estimate");	//line -> "ancID" -> Triplet
		EstimateTable 	est_table_qtls_2d = new EstimateTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_2D) + "_Estimate");	//line -> "ancID" -> Triplet
		EstimateTable 	est_table_qtls_3d = new EstimateTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_3D) + "_Estimate");	//line -> "ancID" -> Triplet
		EstimateTable 	est_table_qtqpls_2d = new EstimateTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_2D) + "_Estimate");	//line -> "ancID" -> Triplet
		EstimateTable 	est_table_qtqpls_3d = new EstimateTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_3D) + "_Estimate");	//line -> "ancID" -> Triplet
		RangeDiffTable  rangedifferr_table_ols_2d = new RangeDiffTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_2D) + "_Range_Diff_Error");  //line -> "ancID_ancID_ancID" -> Double
		RangeDiffTable	rangedifferr_table_ols_3d = new RangeDiffTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_3D) + "_Range_Diff_Error");  //line -> "ancID_ancID_ancID" -> Double	
		RangeDiffTable  rangedifferr_table_qtls_2d = new RangeDiffTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_2D) + "_Range_Diff_Error");   //line -> "ancID_ancID_ancID" -> Double
		RangeDiffTable	rangedifferr_table_qtls_3d = new RangeDiffTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_3D) + "_Range_Diff_Error");   //line -> "ancID_ancID_ancID" -> Double	
		RangeDiffTable  rangedifferr_table_qtqpls_2d = new RangeDiffTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_2D) + "_Range_Diff_Error"); //line -> "ancID_ancID_ancID" -> Double
		RangeDiffTable	rangedifferr_table_qtqpls_3d = new RangeDiffTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_3D) + "_Range_Diff_Error"); //line -> "ancID_ancID_ancID" -> Double	
		double 			xest_ols_2d=0,    yest_ols_2d=0,    xest_ols_3d=0,    yest_ols_3d=0,    zest_ols_3d=0, 
						xest_qtls_2d=0,   yest_qtls_2d=0,   xest_qtls_3d=0,   yest_qtls_3d=0,   zest_qtls_3d=0,
						xest_qtqpls_2d=0, yest_qtqpls_2d=0, xest_qtqpls_3d=0, yest_qtqpls_3d=0, zest_qtqpls_3d=0;
		ResultTable result_ols_ra_2d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_RA_2D) + "_Result");
		ResultTable result_ols_ra_3d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_RA_3D) + "_Result");
		ResultTable result_ols_nra_2d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_NRA_2D) + "_Result");
		ResultTable result_ols_nra_3d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_NRA_3D) + "_Result");
		ResultTable result_ols_aa_2d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_AA_2D) + "_Result");
		ResultTable result_ols_aa_3d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_AA_3D) + "_Result");
		ResultTable result_qtls_ra_2d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_RA_2D) + "_Result");
		ResultTable result_qtls_ra_3d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_RA_3D) + "_Result");
		ResultTable result_qtls_nra_2d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_NRA_2D) + "_Result");
		ResultTable result_qtls_nra_3d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_NRA_3D) + "_Result");
		ResultTable result_qtls_aa_2d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_AA_2D) + "_Result");
		ResultTable result_qtls_aa_3d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_AA_3D) + "_Result");
		ResultTable result_qtqpls_ra_2d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_RA_2D) + "_Result");
		ResultTable result_qtqpls_ra_3d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_RA_3D) + "_Result");
		ResultTable result_qtqpls_nra_2d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_NRA_2D) + "_Result");
		ResultTable result_qtqpls_nra_3d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_NRA_3D) + "_Result");
		ResultTable result_qtqpls_aa_2d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_AA_2D) + "_Result");
		ResultTable result_qtqpls_aa_3d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_AA_3D) + "_Result");
		EuclideanErrorTable err_ols_ra_2d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_RA_2D) + "_Error");
		EuclideanErrorTable	err_ols_ra_3d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_RA_3D) + "_Error");
		EuclideanErrorTable err_ols_nra_2d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_NRA_2D) + "_Error");
		EuclideanErrorTable err_ols_nra_3d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_NRA_3D) + "_Error");
		EuclideanErrorTable err_ols_aa_2d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_AA_2D) + "_Error");
		EuclideanErrorTable	err_ols_aa_3d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_OLS_AA_3D) + "_Error");
		EuclideanErrorTable err_qtls_ra_2d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_RA_2D) + "_Error");
		EuclideanErrorTable	err_qtls_ra_3d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_RA_3D) + "_Error");
		EuclideanErrorTable err_qtls_nra_2d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_NRA_2D) + "_Error");
		EuclideanErrorTable	err_qtls_nra_3d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_NRA_3D) + "_Error");
		EuclideanErrorTable err_qtls_aa_2d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_AA_2D) + "_Error");
		EuclideanErrorTable	err_qtls_aa_3d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_QTLS_AA_3D) + "_Error");
		EuclideanErrorTable err_qtqpls_ra_2d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_RA_2D) + "_Error");
		EuclideanErrorTable	err_qtqpls_ra_3d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_RA_3D) + "_Error");
		EuclideanErrorTable err_qtqpls_nra_2d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_NRA_2D) +"_Error");
		EuclideanErrorTable	err_qtqpls_nra_3d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_NRA_3D) + "_Error");
		EuclideanErrorTable err_qtqpls_aa_2d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_AA_2D) + "_Error");
		EuclideanErrorTable	err_qtqpls_aa_3d  = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHO_QTQPLS_AA_3D) + "_Error");
		
		
		//compute required static values
		int ancnum = groundtruth.size()-1;
		Triplet tag = groundtruth.get(groundtruth.size()-1);
		
		
		//start computing estimation per line
		for (int n=1; n<=linecount; n++) {
			//loop for each anchor to be the base anchor for calculation EXCEPT for the simulated anchor (i.e., the 5th anchor)
			for (int i=0; i<ancnum-1; i++) { 
				Triplet tp_ols_2d = TDOA.computeAnchorBasedCHOLSEstimate (rangedifftable.get(n), (List<Triplet>) groundtruth.subList(0, ancnum), i, Dimension.TWOD);
				Triplet tp_ols_3d = TDOA.computeAnchorBasedCHOLSEstimate (rangedifftable.get(n), (List<Triplet>) groundtruth.subList(0, ancnum), i, Dimension.THREED);
				Triplet tp_qtls_2d = TDOA.computeAnchorBasedCHQTLSEstimate (rangedifftable.get(n), (List<Triplet>) groundtruth.subList(0, ancnum), i, Dimension.TWOD);
				Triplet tp_qtls_3d = TDOA.computeAnchorBasedCHQTLSEstimate (rangedifftable.get(n), (List<Triplet>) groundtruth.subList(0, ancnum), i, Dimension.THREED);
				Triplet tp_qtqpls_2d = TDOA.computeAnchorBasedCHQTQPLSCHEstimate (rangedifftable.get(n), (List<Triplet>) groundtruth.subList(0, ancnum), i, Dimension.TWOD);
				Triplet tp_qtqpls_3d = TDOA.computeAnchorBasedCHQTQPLSCHEstimate (rangedifftable.get(n), (List<Triplet>) groundtruth.subList(0, ancnum), i, Dimension.THREED);
				String istr = Integer.toString(i);
				
				if (tp_ols_2d != null) {
					est_table_ols_2d.put(n, istr, tp_ols_2d);
					xest_ols_2d = tp_ols_2d.getX();
					yest_ols_2d = tp_ols_2d.getY();
				}	
				
				if (tp_ols_3d != null) {
					est_table_ols_3d.put(n, istr, tp_ols_3d);
					xest_ols_3d = tp_ols_3d.getX();
					yest_ols_3d = tp_ols_3d.getY();
					zest_ols_3d = tp_ols_3d.getZ();
				}
				
				if (tp_qtls_2d != null) {
					est_table_qtls_2d.put(n, istr, tp_qtls_2d);
					xest_qtls_2d = tp_qtls_2d.getX();
					yest_qtls_2d = tp_qtls_2d.getY();
				}
				
				if (tp_qtls_3d != null) {
					est_table_qtls_3d.put(n, istr, tp_qtls_3d);
					xest_qtls_3d = tp_qtls_3d.getX();
					yest_qtls_3d = tp_qtls_3d.getY();
					zest_qtls_3d = tp_qtls_3d.getZ();
				}
				
				if (tp_qtqpls_2d != null) {
					est_table_qtqpls_2d.put(n, istr, tp_qtqpls_2d);
					xest_qtqpls_2d = tp_qtqpls_2d.getX();
					yest_qtqpls_2d = tp_qtqpls_2d.getY();
				}
				
				if (tp_qtqpls_3d != null) {
					est_table_qtqpls_3d.put(n, istr, tp_qtqpls_3d);
					xest_qtqpls_3d = tp_qtqpls_3d.getX();
					yest_qtqpls_3d = tp_qtqpls_3d.getY();
					zest_qtqpls_3d = tp_qtqpls_3d.getZ();
				}
				
				
				//calculate range difference error
				for (int ii=0; ii<ancnum-1; ii++) {
					Triplet tp = groundtruth.get(ii);
					double xii = tp.getX();
					double yii = tp.getY();
					double zii = tp.getZ();
					
					for (int jj=ii+1; jj<ancnum; jj++) {
						tp = groundtruth.get(jj);
						double xjj = tp.getX();
						double yjj = tp.getY();
						double zjj = tp.getZ();
						
						if (tp_ols_2d != null) {
							double rangecalols2d_ii = Math.sqrt(Math.pow(xest_ols_2d-xii, 2) + Math.pow(yest_ols_2d-yii, 2));
							double rangecalols2d_jj = Math.sqrt(Math.pow(xest_ols_2d-xjj, 2) + Math.pow(yest_ols_2d-yjj, 2));
							double rangediffcalols2d_ii_jj = rangecalols2d_ii - rangecalols2d_jj; 
							double rangedifferrols2d = Math.abs(rangedifftable.get(n, ii+"_"+jj) - rangediffcalols2d_ii_jj);
							rangedifferr_table_ols_2d.put(n, i+"_"+ii+"_"+jj, rangedifferrols2d);
						}
						
						if (tp_ols_3d != null) {
							double rangecalols3d_ii = Math.sqrt(Math.pow(xest_ols_3d-xii, 2) + Math.pow(yest_ols_3d-yii, 2) + Math.pow(zest_ols_3d-zii, 2));
							double rangecalols3d_jj = Math.sqrt(Math.pow(xest_ols_3d-xjj, 2) + Math.pow(yest_ols_3d-yjj, 2) + Math.pow(zest_ols_3d-zjj, 2));
							double rangediffcalols3d_ii_jj = rangecalols3d_ii - rangecalols3d_jj;
							double rangedifferrols3d = Math.abs(rangedifftable.get(n, ii+"_"+jj) - rangediffcalols3d_ii_jj);
							rangedifferr_table_ols_3d.put(n, i+"_"+ii+"_"+jj, rangedifferrols3d);
						}
						
						if (tp_qtls_2d != null) {
							double rangecalqtls2d_ii = Math.sqrt(Math.pow(xest_qtls_2d-xii, 2) + Math.pow(yest_qtls_2d-yii, 2));
							double rangecalqtls2d_jj = Math.sqrt(Math.pow(xest_qtls_2d-xjj, 2) + Math.pow(yest_qtls_2d-yjj, 2));
							double rangediffcalqtls2d_ii_jj = rangecalqtls2d_ii - rangecalqtls2d_jj; 
							double rangedifferrqtls2d = Math.abs(rangedifftable.get(n, ii+"_"+jj) - rangediffcalqtls2d_ii_jj);
							rangedifferr_table_qtls_2d.put(n, i+"_"+ii+"_"+jj, rangedifferrqtls2d);
						}
							
						if (tp_qtls_3d != null) {
							double rangecalqtls3d_ii = Math.sqrt(Math.pow(xest_qtls_3d-xii, 2) + Math.pow(yest_qtls_3d-yii, 2) + Math.pow(zest_qtls_3d-zii, 2));
							double rangecalqtls3d_jj = Math.sqrt(Math.pow(xest_qtls_3d-xjj, 2) + Math.pow(yest_qtls_3d-yjj, 2) + Math.pow(zest_qtls_3d-zjj, 2));
							double rangediffcalqtls3d_ii_jj = rangecalqtls3d_ii - rangecalqtls3d_jj;
							double rangedifferrqtls3d = Math.abs(rangedifftable.get(n, ii+"_"+jj) - rangediffcalqtls3d_ii_jj);
							rangedifferr_table_qtls_3d.put(n, i+"_"+ii+"_"+jj, rangedifferrqtls3d);
						}
						
						if (tp_qtqpls_2d != null) {	
							double rangecalqtqpls2d_ii = Math.sqrt(Math.pow(xest_qtqpls_2d-xii, 2) + Math.pow(yest_qtqpls_2d-yii, 2));
							double rangecalqtqpls2d_jj = Math.sqrt(Math.pow(xest_qtqpls_2d-xjj, 2) + Math.pow(yest_qtqpls_2d-yjj, 2));
							double rangediffcalqtqpls2d_ii_jj = rangecalqtqpls2d_ii - rangecalqtqpls2d_jj; 
							double rangedifferrqtqpls2d = Math.abs(rangedifftable.get(n, ii+"_"+jj) - rangediffcalqtqpls2d_ii_jj);
							rangedifferr_table_qtqpls_2d.put(n, i+"_"+ii+"_"+jj, rangedifferrqtqpls2d);
						}
						
						if (tp_qtqpls_3d != null) {
							double rangecalqtqpls3d_ii = Math.sqrt(Math.pow(xest_qtqpls_3d-xii, 2) + Math.pow(yest_qtqpls_3d-yii, 2) + Math.pow(zest_qtqpls_3d-zii, 2));
							double rangecalqtqpls3d_jj = Math.sqrt(Math.pow(xest_qtqpls_3d-xjj, 2) + Math.pow(yest_qtqpls_3d-yjj, 2) + Math.pow(zest_qtqpls_3d-zjj, 2));
							double rangediffcalqtqpls3d_ii_jj = rangecalqtqpls3d_ii - rangecalqtqpls3d_jj;
							double rangedifferrqtqpls3d = Math.abs(rangedifftable.get(n, ii+"_"+jj) - rangediffcalqtqpls3d_ii_jj);
							rangedifferr_table_qtqpls_3d.put(n, i+"_"+ii+"_"+jj, rangedifferrqtqpls3d);
						}
					}//for(int jj)
				}//for(int ii) 
			}//for(int i)
			
			Triplet tp;
			
			/*===== RA =====*/
			if (est_table_ols_2d.containsKey(n) && (tp=chooseByRelatedAnchors(ancnum, est_table_ols_2d.get(n), rangedifferr_table_ols_2d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_ols_ra_2d.put(n, tp);
				err_ols_ra_2d.put(n, err);
			}
			
			if (est_table_ols_3d.containsKey(n) && (tp=chooseByRelatedAnchors(ancnum, est_table_ols_3d.get(n), rangedifferr_table_ols_3d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp);
				result_ols_ra_3d.put(n, tp);
				err_ols_ra_3d.put(n, err);
			}
			
			if (est_table_qtls_2d.containsKey(n) && (tp=chooseByRelatedAnchors(ancnum, est_table_qtls_2d.get(n), rangedifferr_table_qtls_2d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_qtls_ra_2d.put(n, tp);
				err_qtls_ra_2d.put(n, err);
			}
			
			if (est_table_qtls_3d.containsKey(n) && (tp=chooseByRelatedAnchors(ancnum, est_table_qtls_3d.get(n), rangedifferr_table_qtls_3d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp);
				result_qtls_ra_3d.put(n, tp);
				err_qtls_ra_3d.put(n, err);
			}
			
			if (est_table_qtqpls_2d.containsKey(n) && (tp=chooseByRelatedAnchors(ancnum, est_table_qtqpls_2d.get(n), rangedifferr_table_qtqpls_2d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_qtqpls_ra_2d.put(n, tp);
				err_qtqpls_ra_2d.put(n, err);
			}
			
			if (est_table_qtqpls_3d.containsKey(n) && (tp=chooseByRelatedAnchors(ancnum, est_table_qtqpls_3d.get(n), rangedifferr_table_qtqpls_3d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp);
				result_qtqpls_ra_3d.put(n, tp);
				err_qtqpls_ra_3d.put(n, err);
			}
			
			/*===== NRA =====*/
			if (est_table_ols_2d.containsKey(n) && (tp=chooseByNonRelatedAnchors(ancnum, est_table_ols_2d.get(n), rangedifferr_table_ols_2d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_ols_nra_2d.put(n, tp);
				err_ols_nra_2d.put(n, err);
			}
			
			if (est_table_ols_3d.containsKey(n) && (tp=chooseByNonRelatedAnchors(ancnum, est_table_ols_3d.get(n), rangedifferr_table_ols_3d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp);
				result_ols_nra_3d.put(n, tp);
				err_ols_nra_3d.put(n, err);
			}
			
			if (est_table_qtls_2d.containsKey(n) && (tp=chooseByNonRelatedAnchors(ancnum, est_table_qtls_2d.get(n), rangedifferr_table_qtls_2d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_qtls_nra_2d.put(n, tp);
				err_qtls_nra_2d.put(n, err);
			}
			
			if (est_table_qtls_3d.containsKey(n) && (tp=chooseByNonRelatedAnchors(ancnum, est_table_qtls_3d.get(n), rangedifferr_table_qtls_3d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp);
				result_qtls_nra_3d.put(n, tp);
				err_qtls_nra_3d.put(n, err);
			}
			
			if (est_table_qtqpls_2d.containsKey(n) && (tp=chooseByNonRelatedAnchors(ancnum, est_table_qtqpls_2d.get(n), rangedifferr_table_qtqpls_2d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_qtqpls_nra_2d.put(n, tp);
				err_qtqpls_nra_2d.put(n, err);
			}
			
			if (est_table_qtqpls_3d.containsKey(n) && (tp=chooseByNonRelatedAnchors(ancnum, est_table_qtqpls_3d.get(n), rangedifferr_table_qtqpls_3d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp);
				result_qtqpls_nra_3d.put(n, tp);
				err_qtqpls_nra_3d.put(n, err);
			}
			
			/*===== AA =====*/
			if (est_table_ols_2d.containsKey(n) && (tp=chooseByAllAnchors(ancnum, est_table_ols_2d.get(n), rangedifferr_table_ols_2d.get(n))) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_ols_aa_2d.put(n, tp);
				err_ols_aa_2d.put(n, err);
			}
			if (est_table_ols_3d.containsKey(n) && (tp=chooseByAllAnchors(ancnum, est_table_ols_3d.get(n), rangedifferr_table_ols_3d.get(n))) != null) {
				double err = tag.rangeWith(tp);
				result_ols_aa_3d.put(n, tp);
				err_ols_aa_3d.put(n, err);
			}
			
			if (est_table_qtls_2d.containsKey(n) && (tp=chooseByAllAnchors(ancnum, est_table_qtls_2d.get(n), rangedifferr_table_qtls_2d.get(n))) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_qtls_aa_2d.put(n, tp);
				err_qtls_aa_2d.put(n, err);
			}
			if (est_table_qtls_3d.containsKey(n) && (tp=chooseByAllAnchors(ancnum, est_table_qtls_3d.get(n), rangedifferr_table_qtls_3d.get(n))) != null) {
				double err = tag.rangeWith(tp);
				result_qtls_aa_3d.put(n, tp);
				err_qtls_aa_3d.put(n, err);
			}
			
			if (est_table_qtqpls_2d.containsKey(n) && (tp=chooseByAllAnchors(ancnum, est_table_qtqpls_2d.get(n), rangedifferr_table_qtqpls_2d.get(n))) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_qtqpls_aa_2d.put(n, tp);
				err_qtqpls_aa_2d.put(n, err);
			}
			if (est_table_qtqpls_3d.containsKey(n) && (tp=chooseByAllAnchors(ancnum, est_table_qtqpls_3d.get(n), rangedifferr_table_qtqpls_3d.get(n))) != null) {
				double err = tag.rangeWith(tp);
				result_qtqpls_aa_3d.put(n, tp);
				err_qtqpls_aa_3d.put(n, err);
			}
		}//for(n<linecount)

		
		//print headers and results
		PrintWriter chout = new PrintWriter(new File(outfilename+".csv"));
		for (int n=0; n<=linecount; n++) {
			String str = "";
			for (int i=0; i<ancnum-1; i++) {
				//estimation coordinates
				str += 	est_table_ols_2d.printCSV(n, Integer.toString(i), Dimension.TWOD)
					+ 	est_table_ols_3d.printCSV(n, Integer.toString(i), Dimension.THREED) 
					+ 	est_table_qtls_2d.printCSV(n, Integer.toString(i), Dimension.TWOD)
					+ 	est_table_qtls_3d.printCSV(n, Integer.toString(i), Dimension.THREED) 
					+ 	est_table_qtqpls_2d.printCSV(n, Integer.toString(i), Dimension.TWOD)
					+ 	est_table_qtqpls_3d.printCSV(n, Integer.toString(i), Dimension.THREED) ;
				
				//range difference error
				for (int ii=0; ii<ancnum-1; ii++) {
					for (int jj=ii+1; jj<ancnum; jj++) {
						String strindex = i+"_"+ii+"_"+jj;
						
						str +=	rangedifferr_table_ols_2d.printCSV(n, strindex)
							+	rangedifferr_table_ols_3d.printCSV(n, strindex)
							+	rangedifferr_table_qtls_2d.printCSV(n, strindex)
							+	rangedifferr_table_qtls_3d.printCSV(n, strindex)
							+	rangedifferr_table_qtqpls_2d.printCSV(n, strindex)
							+	rangedifferr_table_qtqpls_3d.printCSV(n, strindex);
					}//for(int jj)
				}//for(int ii)
			}//for(int i)
		
			
			//final result
			str +=	result_ols_ra_2d.printCSV(n, Dimension.TWOD)
				+	result_ols_nra_2d.printCSV(n, Dimension.TWOD)
				+	result_ols_aa_2d.printCSV(n, Dimension.TWOD)		
				+ 	result_qtls_ra_2d.printCSV(n, Dimension.TWOD)
				+	result_qtls_nra_2d.printCSV(n, Dimension.TWOD)
				+	result_qtls_aa_2d.printCSV(n, Dimension.TWOD)
				+	result_qtqpls_ra_2d.printCSV(n, Dimension.TWOD)
				+	result_qtqpls_nra_2d.printCSV(n, Dimension.TWOD)
				+	result_qtqpls_aa_2d.printCSV(n, Dimension.TWOD)
				+	result_ols_ra_3d.printCSV(n, Dimension.THREED)
				+	result_ols_nra_3d.printCSV(n, Dimension.THREED)
				+	result_ols_aa_3d.printCSV(n, Dimension.THREED)
				+	result_qtls_ra_3d.printCSV(n, Dimension.THREED)
				+	result_qtls_nra_3d.printCSV(n, Dimension.THREED)
				+	result_qtls_aa_3d.printCSV(n, Dimension.THREED)
				+	result_qtqpls_ra_3d.printCSV(n, Dimension.THREED)
				+	result_qtqpls_nra_3d.printCSV(n, Dimension.THREED)
				+	result_qtqpls_aa_3d.printCSV(n, Dimension.THREED);
			
			
			//range_error_[OLS|QTLS|QTQPLS]_[RA|NRA|AA]_[2D|3D]
			str +=	err_ols_ra_2d.printCSV(n)
				+	err_ols_nra_2d.printCSV(n)
				+	err_ols_aa_2d.printCSV(n)
				+	err_qtls_ra_2d.printCSV(n)
				+	err_qtls_nra_2d.printCSV(n)
				+	err_qtls_aa_2d.printCSV(n)
				+	err_qtqpls_ra_2d.printCSV(n)
				+	err_qtqpls_nra_2d.printCSV(n)
				+	err_qtqpls_aa_2d.printCSV(n)
				+	err_ols_ra_3d.printCSV(n)
				+	err_ols_nra_3d.printCSV(n)
				+	err_ols_aa_3d.printCSV(n)
				+	err_qtls_ra_3d.printCSV(n)
				+	err_qtls_nra_3d.printCSV(n)
				+	err_qtls_aa_3d.printCSV(n)
				+	err_qtqpls_ra_3d.printCSV(n)
				+	err_qtqpls_nra_3d.printCSV(n)
				+	err_qtqpls_aa_3d.printCSV(n);
			
			chout.println(str);		
		}//for(int n)
		
		chout.flush();
		chout.close();
	}
	
	
	
	void genCHExtOut (String outfilename, ArrayList<Triplet> groundtruth, RangeDiffTable rangedifftable) throws Exception {
		EstimateTable 	est_table_qtls_2d = new EstimateTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_2D) + "_Estimate");	//line -> "ancID" -> Triplet
		EstimateTable 	est_table_qtls_3d = new EstimateTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_3D) + "_Estimate");	//line -> "ancID" -> Triplet
		RangeDiffTable  rangedifferr_table_qtls_2d = new RangeDiffTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_2D) + "_Range_Diff_Error");   //line -> "ancID_ancID_ancID" -> Double
		RangeDiffTable	rangedifferr_table_qtls_3d = new RangeDiffTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_3D) + "_Range_Diff_Error");   //line -> "ancID_ancID_ancID" -> Double	
		ResultTable 	result_qtls_ra_2d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_RA_2D) + "_Result");
		ResultTable 	result_qtls_ra_3d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_RA_3D) + "_Result");
		ResultTable 	result_qtls_nra_2d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_NRA_2D) + "_Result");
		ResultTable 	result_qtls_nra_3d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_NRA_3D) + "_Result");
		ResultTable 	result_qtls_aa_2d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_AA_2D) + "_Result");
		ResultTable 	result_qtls_aa_3d = new ResultTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_AA_3D) + "_Result");
		EuclideanErrorTable err_qtls_ra_2d = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_RA_2D) + "_Error");
		EuclideanErrorTable	err_qtls_ra_3d = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_RA_3D) + "_Error");
		EuclideanErrorTable err_qtls_nra_2d = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_NRA_2D) + "_Error");
		EuclideanErrorTable	err_qtls_nra_3d = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_NRA_3D) + "_Error");
		EuclideanErrorTable err_qtls_aa_2d = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_AA_2D) + "_Error");
		EuclideanErrorTable	err_qtls_aa_3d = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.CHANHOEXT_QTLS_AA_3D) + "_Error");
		
		
		//compute required static values
		int ancnum = groundtruth.size()-1;
		Triplet tag = groundtruth.get(groundtruth.size()-1);
		
		PrintWriter qtls_2d_dbg_file = new PrintWriter(new FileWriter(outfilename+"_qtls_2d_dbg.txt", true));
		PrintWriter qtls_3d_dbg_file = new PrintWriter(new FileWriter(outfilename+"_qtls_3d_dbg.txt", true));
		
		//start computing estimation per line
		for (int n=1; n<=linecount; n++) {
			qtls_2d_dbg_file.println("===== Line " + n + " =====");
			qtls_3d_dbg_file.println("===== Line " + n + " =====");
			
			double[][] rangediff = new double[ancnum][ancnum];
			Triplet[] anchors = new Triplet[ancnum];
			anchors = groundtruth.subList(0, ancnum).toArray(anchors);
			rangeDiffSubTableToDoubleArray(rangediff, rangedifftable.get(n));
			Triplet[] tp_qtls_2d = TDOA.computeSystemWideCHQTLSEstimate (qtls_2d_dbg_file, rangediff, anchors, Dimension.TWOD);
			Triplet[] tp_qtls_3d = TDOA.computeSystemWideCHQTLSEstimate (qtls_3d_dbg_file, rangediff, anchors, Dimension.THREED);
			
			for (int i=0; i<ancnum; i++) {
				//**** THE LAST ELEMENT OF tp_qtls_2d AND tp_qtls_3d IS u1 MATRIX ****//
				double xest_qtls_2d=0, yest_qtls_2d=0, xest_qtls_3d=0, yest_qtls_3d=0, zest_qtls_3d=0;
				
				if (tp_qtls_2d[i] != null) {
					est_table_qtls_2d.put(n, String.valueOf(i), tp_qtls_2d[i]);
					xest_qtls_2d = tp_qtls_2d[i].getX();
					yest_qtls_2d = tp_qtls_2d[i].getY();
				}
			
				if (tp_qtls_3d[i] != null) {
					est_table_qtls_3d.put(n, String.valueOf(i), tp_qtls_3d[i]);
					xest_qtls_3d = tp_qtls_3d[i].getX();
					yest_qtls_3d = tp_qtls_3d[i].getY();
					zest_qtls_3d = tp_qtls_3d[i].getZ();
				}
						
				//calculate range difference error
				for (int ii=0; ii<ancnum-1; ii++) {
					Triplet tp = groundtruth.get(ii);
					double xii = tp.getX();
					double yii = tp.getY();
					double zii = tp.getZ();
					
					for (int jj=ii+1; jj<ancnum; jj++) {
						tp = groundtruth.get(jj);
						double xjj = tp.getX();
						double yjj = tp.getY();
						double zjj = tp.getZ();
						
						if (tp_qtls_2d[i] != null) {
							double rangecalqtls2d_ii = Math.sqrt(Math.pow(xest_qtls_2d-xii, 2) + Math.pow(yest_qtls_2d-yii, 2));
							double rangecalqtls2d_jj = Math.sqrt(Math.pow(xest_qtls_2d-xjj, 2) + Math.pow(yest_qtls_2d-yjj, 2));
							double rangediffcalqtls2d_ii_jj = rangecalqtls2d_ii - rangecalqtls2d_jj; 
							double rangedifferrqtls2d = Math.abs(rangedifftable.get(n, ii+"_"+jj) - rangediffcalqtls2d_ii_jj);
							rangedifferr_table_qtls_2d.put(n, i+"_"+ii+"_"+jj, rangedifferrqtls2d);
						}
							
						if (tp_qtls_3d[i] != null) {
							double rangecalqtls3d_ii = Math.sqrt(Math.pow(xest_qtls_3d-xii, 2) + Math.pow(yest_qtls_3d-yii, 2) + Math.pow(zest_qtls_3d-zii, 2));
							double rangecalqtls3d_jj = Math.sqrt(Math.pow(xest_qtls_3d-xjj, 2) + Math.pow(yest_qtls_3d-yjj, 2) + Math.pow(zest_qtls_3d-zjj, 2));
							double rangediffcalqtls3d_ii_jj = rangecalqtls3d_ii - rangecalqtls3d_jj;
							double rangedifferrqtls3d = Math.abs(rangedifftable.get(n, ii+"_"+jj) - rangediffcalqtls3d_ii_jj);
							rangedifferr_table_qtls_3d.put(n, i+"_"+ii+"_"+jj, rangedifferrqtls3d);
						}
					}//for(ii+1<jj<ancnum)
				}//for(0<ii<ancnum-1) 
			}//for(0<i<ancnum)
			
			Triplet tp;
			
			/*===== RA =====*/
			if (est_table_qtls_2d.containsKey(n) && (tp=chooseByRelatedAnchors(ancnum, est_table_qtls_2d.get(n), rangedifferr_table_qtls_2d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_qtls_ra_2d.put(n, tp);
				err_qtls_ra_2d.put(n, err);
			}
			
			if (est_table_qtls_3d.containsKey(n) && (tp=chooseByRelatedAnchors(ancnum, est_table_qtls_3d.get(n), rangedifferr_table_qtls_3d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp);
				result_qtls_ra_3d.put(n, tp);
				err_qtls_ra_3d.put(n, err);
			}
			
			
			/*===== NRA =====*/
			if (est_table_qtls_2d.containsKey(n) && (tp=chooseByNonRelatedAnchors(ancnum, est_table_qtls_2d.get(n), rangedifferr_table_qtls_2d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_qtls_nra_2d.put(n, tp);
				err_qtls_nra_2d.put(n, err);
			}
			
			if (est_table_qtls_3d.containsKey(n) && (tp=chooseByNonRelatedAnchors(ancnum, est_table_qtls_3d.get(n), rangedifferr_table_qtls_3d.get(n), TDOA.Variant.CHANHO)) != null) {
				double err = tag.rangeWith(tp);
				result_qtls_nra_3d.put(n, tp);
				err_qtls_nra_3d.put(n, err);
			}
			
			
			/*===== AA =====*/
			if (est_table_qtls_2d.containsKey(n) && (tp=chooseByAllAnchors(ancnum, est_table_qtls_2d.get(n), rangedifferr_table_qtls_2d.get(n))) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_qtls_aa_2d.put(n, tp);
				err_qtls_aa_2d.put(n, err);
			}
			if (est_table_qtls_3d.containsKey(n) && (tp=chooseByAllAnchors(ancnum, est_table_qtls_3d.get(n), rangedifferr_table_qtls_3d.get(n))) != null) {
				double err = tag.rangeWith(tp);
				result_qtls_aa_3d.put(n, tp);
				err_qtls_aa_3d.put(n, err);
			}
		}//for(n<linecount)

		qtls_2d_dbg_file.close();
		qtls_3d_dbg_file.close();
				
		
		//print headers and results
		PrintWriter chout = new PrintWriter(new File(outfilename+".csv"));
		for (int n=0; n<=linecount; n++) {
			String str = "";
			for (int i=0; i<ancnum; i++) {
				//estimation coordinates
				str += 	est_table_qtls_2d.printCSV(n, Integer.toString(i), Dimension.TWOD)
					+ 	est_table_qtls_3d.printCSV(n, Integer.toString(i), Dimension.THREED) ;
				
				//range difference error
				for (int ii=0; ii<ancnum-1; ii++) {
					for (int jj=ii+1; jj<ancnum; jj++) {
						String strindex = i+"_"+ii+"_"+jj;
						
						str +=	rangedifferr_table_qtls_2d.printCSV(n, strindex)
							+	rangedifferr_table_qtls_3d.printCSV(n, strindex);
					}//for(ii+1<jj<ancnum)
				}//for(0<ii<ancnum-1)
			}//for(0<i<ancnum)
		
			
			//final result
			str +=	result_qtls_ra_2d.printCSV(n, Dimension.TWOD)
				+	result_qtls_nra_2d.printCSV(n, Dimension.TWOD)
				+	result_qtls_aa_2d.printCSV(n, Dimension.TWOD)
				+	result_qtls_ra_3d.printCSV(n, Dimension.THREED)
				+	result_qtls_nra_3d.printCSV(n, Dimension.THREED)
				+	result_qtls_aa_3d.printCSV(n, Dimension.THREED);
			
			
			//range_error_[OLS|QTLS|QTQPLS]_[RA|NRA|AA]_[2D|3D]
			str +=	err_qtls_ra_2d.printCSV(n)
				+	err_qtls_nra_2d.printCSV(n)
				+	err_qtls_aa_2d.printCSV(n)
				+	err_qtls_ra_3d.printCSV(n)
				+	err_qtls_nra_3d.printCSV(n)
				+	err_qtls_aa_3d.printCSV(n);
			
			chout.println(str);		
		}//for(int n)
		
		chout.flush();
		chout.close();
	}	
	
	
//=============================================== WIKI ===================================================
	
	void genWikiOut (String outfilename, ArrayList<Triplet> groundtruth, RangeDiffTable rangedifftable) throws Exception {
		EstimateTable 	esttable2d = new EstimateTable(TDOA.toString(TDOA.Variant.WIKI_2D) + "_Estimate");	//line -> "ancID" -> Triplet
		EstimateTable 	esttable3d = new EstimateTable(TDOA.toString(TDOA.Variant.WIKI_3D) + "_Estimate");	//line -> "ancID" -> Triplet
		RangeDiffTable  rangedifferrtable2d = new RangeDiffTable(TDOA.toString(TDOA.Variant.WIKI_2D) + "_Range_Diff_Error"); //line -> "ancID_ancID_ancID" -> Double
		RangeDiffTable  rangedifferrtable3d = new RangeDiffTable(TDOA.toString(TDOA.Variant.WIKI_3D) + "_Range_Diff_Error"); //line -> "ancID_ancID_ancID" -> Double
		ResultTable 	result_ra_2d = new ResultTable(TDOA.toString(TDOA.Variant.WIKI_RA_2D) + "_Result");
		ResultTable 	result_ra_3d = new ResultTable(TDOA.toString(TDOA.Variant.WIKI_RA_3D) + "_Result");
		ResultTable 	result_nra_2d = new ResultTable(TDOA.toString(TDOA.Variant.WIKI_NRA_2D) + "_Result");
		ResultTable 	result_nra_3d = new ResultTable(TDOA.toString(TDOA.Variant.WIKI_NRA_3D) + "_Result");
		ResultTable 	result_aa_2d = new ResultTable(TDOA.toString(TDOA.Variant.WIKI_AA_2D) + "_Result");
		ResultTable 	result_aa_3d = new ResultTable(TDOA.toString(TDOA.Variant.WIKI_AA_3D) + "_Result");
		EuclideanErrorTable err_ra_2d = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.WIKI_RA_2D) + "_Error");
		EuclideanErrorTable	err_ra_3d = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.WIKI_RA_3D) + "_Error");
		EuclideanErrorTable err_nra_2d = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.WIKI_NRA_2D) + "_Error");
		EuclideanErrorTable err_nra_3d = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.WIKI_NRA_3D) + "_Error");
		EuclideanErrorTable err_aa_2d = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.WIKI_AA_2D) + "_Error");
		EuclideanErrorTable err_aa_3d = new EuclideanErrorTable(TDOA.toString(TDOA.Variant.WIKI_AA_3D) + "_Error");
		
		//start computing estimation per line
		int ancnum = groundtruth.size()-1;
		Triplet tag = groundtruth.get(groundtruth.size()-1);
		
		for (int n=1; n<=linecount; n++) {
			//loop for each (real**) anchor to be the base anchor for calculation (hence, i iterates from 0 to ancnum-1)
			for (int i=0; i<ancnum-1; i++) { 
				for (int j=0; j<ancnum; j++) {
					if (i != j) {
						Triplet tp2d = TDOA.computeWikiEstimate (rangedifftable.get(n), (List<Triplet>) groundtruth.subList(0, ancnum), i, j, Dimension.TWOD);
						Triplet tp3d = TDOA.computeWikiEstimate (rangedifftable.get(n), (List<Triplet>) groundtruth.subList(0, ancnum), i, j, Dimension.THREED);
						
						double xest2d=0, yest2d=0, xest3d=0, yest3d=0, zest3d=0;
						if (tp2d != null) {
							esttable2d.put(n, i+"_"+j, tp2d);
							xest2d = tp2d.getX();
							yest2d = tp2d.getY();
						}
						
						if (tp3d != null) {
							esttable3d.put(n, i+"_"+j, tp3d);
							xest3d = tp3d.getX();
							yest3d = tp3d.getY();
							zest3d = tp3d.getZ();
						}
						
						
						for (int ii=0; ii<ancnum-1; ii++) {
							Triplet tp = groundtruth.get(ii);
							double xii = tp.getX();
							double yii = tp.getY();
							double zii = tp.getZ();
							
							for (int jj=ii+1; jj<ancnum; jj++) {
								tp = groundtruth.get(jj);
								double xjj = tp.getX();
								double yjj = tp.getY();
								double zjj = tp.getZ();
								
								if (tp2d != null) {
									double rangecal2d_ii = Math.sqrt(Math.pow(xest2d-xii, 2) + Math.pow(yest2d-yii, 2));
									double rangecal2d_jj = Math.sqrt(Math.pow(xest2d-xjj, 2) + Math.pow(yest2d-yjj, 2));
									double rangediffcal2d_ii_jj = rangecal2d_ii - rangecal2d_jj;
									double rangedifferr2d = Math.abs(rangedifftable.get(n, ii+"_"+jj) - rangediffcal2d_ii_jj);
									rangedifferrtable2d.put(n, i+"_"+j+"_"+ii+"_"+jj, rangedifferr2d);
								}
								if (tp3d != null) {
									double rangecal3d_ii = Math.sqrt(Math.pow(xest3d-xii, 2) + Math.pow(yest3d-yii, 2) + Math.pow(zest3d-zii, 2));
									double rangecal3d_jj = Math.sqrt(Math.pow(xest3d-xjj, 2) + Math.pow(yest3d-yjj, 2) + Math.pow(zest3d-zjj, 2));
									double rangediffcal3d_ii_jj = rangecal3d_ii - rangecal3d_jj;
									double rangedifferr3d = Math.abs(rangedifftable.get(n, ii+"_"+jj) - rangediffcal3d_ii_jj);
									rangedifferrtable3d.put(n, i+"_"+j+"_"+ii+"_"+jj, rangedifferr3d);
								}
							}//for(int jj)
						}//for(int ii)
					}//if(i!=j)
				}//for(int j)
			}//for(int i)

			
			Triplet tp;
			
			// 1. choose from minimal range_diff_err of related anchors in the same line
			if (esttable2d.containsKey(n) && (tp=chooseByRelatedAnchors(ancnum, esttable2d.get(n), rangedifferrtable2d.get(n), TDOA.Variant.WIKI)) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_ra_2d.put(n, tp);
				err_ra_2d.put(n, err);
			}
			if (esttable3d.containsKey(n) && (tp=chooseByRelatedAnchors(ancnum, esttable3d.get(n), rangedifferrtable3d.get(n), TDOA.Variant.WIKI)) != null) {
				double err = tag.rangeWith(tp);
				result_ra_3d.put(n, tp);
				err_ra_3d.put(n, err);
			}
			
			// 2. choose from minimal range_diff_err of non-related anchors in the same line
			if (esttable2d.containsKey(n) && (tp=chooseByNonRelatedAnchors(ancnum, esttable2d.get(n), rangedifferrtable2d.get(n), TDOA.Variant.WIKI)) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_nra_2d.put(n, tp);
				err_nra_2d.put(n, err);
			}
			if (esttable3d.containsKey(n) && (tp=chooseByNonRelatedAnchors(ancnum, esttable3d.get(n), rangedifferrtable3d.get(n), TDOA.Variant.WIKI)) != null) {
				double err = tag.rangeWith(tp);
				result_nra_3d.put(n, tp);
				err_nra_3d.put(n, err);
			}
			
			// 3. choose from minimal 2d_range_diff_err of all anchors in the same line
			if (esttable2d.containsKey(n) && (tp=chooseByAllAnchors(ancnum, esttable2d.get(n), rangedifferrtable2d.get(n))) != null) {
				double err = tag.rangeWith(tp.getX(), tp.getY());
				result_aa_2d.put(n, tp);
				err_aa_2d.put(n, err);
			}
			if (esttable3d.containsKey(n) && (tp=chooseByAllAnchors(ancnum, esttable3d.get(n), rangedifferrtable3d.get(n))) != null) {
				double err = tag.rangeWith(tp);
				result_aa_3d.put(n, tp);
				err_aa_3d.put(n, err);
			}
		}//for (n<linecount)
		
			
		
		//print headers and contents
		PrintWriter wikiout = new PrintWriter(new File(outfilename+".csv"));
		for (int n=0; n<=linecount; n++) {
			String str = "";
			for (int i=0; i<ancnum-1; i++) {
				for (int j=0; j<ancnum; j++) {
					if (i != j) {
						//estimation coordinates
						str += 	esttable2d.printCSV(n, i+"_"+j, Dimension.TWOD) 
							+ 	esttable3d.printCSV(n, i+"_"+j, Dimension.THREED);
						
						//range difference error
						for (int ii=0; ii<ancnum-1; ii++) {
							for (int jj=ii+1; jj<ancnum; jj++) {
								String strindex = i+"_"+j+"_"+ii+"_"+jj;
								
								str +=	rangedifferrtable2d.printCSV(n, strindex)
									+	rangedifferrtable3d.printCSV(n, strindex);
							}//for(int jj)
						}//for(int ii)
					}//if(i!=j)
				}//for(int j)
			}//for(int i)
		
			//result
			str +=	result_ra_2d.printCSV(n, Dimension.TWOD)
				+	result_nra_2d.printCSV(n, Dimension.TWOD)
				+	result_aa_2d.printCSV(n, Dimension.TWOD)
				+	result_ra_3d.printCSV(n, Dimension.THREED)
				+	result_nra_3d.printCSV(n, Dimension.THREED)
				+	result_aa_3d.printCSV(n, Dimension.THREED);
			
			//range error
			str += 	err_ra_2d.printCSV(n)
				+	err_nra_2d.printCSV(n)
				+	err_aa_2d.printCSV(n)
				+	err_ra_3d.printCSV(n)
				+	err_nra_3d.printCSV(n)	
				+	err_aa_3d.printCSV(n);			
						
			wikiout.println(str);		
		}//for(0=n<=linecount)
		
		wikiout.flush();
		wikiout.close();
	}
	
	
	private void rangeDiffSubTableToDoubleArray(double[][] array, Hashtable<String, Double> subtable) {
		int dim = array.length;
		for (int n1=0; n1<dim; n1++) {
			for (int n2=0; n2<dim; n2++) {
				if (n1 != n2) {
					array[n2][n1] = subtable.get(n2+"_"+n1);
				}
			}
		}
	}
}


