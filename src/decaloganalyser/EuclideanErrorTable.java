package decaloganalyser;

import java.util.Enumeration;
import java.util.Hashtable;

public class EuclideanErrorTable {
	Hashtable<Integer, Double> 	table;
	String name;
	
	EuclideanErrorTable (String tablename) {
		table = new Hashtable<Integer, Double>();
		name  = tablename;
	}
	
	
	void put (int line, Double val) {
		table.put(line, val);
	}
	
	
	Enumeration<Double> elements () {
		return table.elements();
	}
	
	
	String printCSV (int line) {
		String str = "";
		
		if (line != 0) {
			//print contents
			if (table.containsKey(line)) {
				str += table.get(line);
			}
			str += ",";
		}
		else {
			//print headers
			str += name + ",";
		}
		
		return str;
	}
}
