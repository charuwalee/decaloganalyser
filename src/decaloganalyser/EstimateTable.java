package decaloganalyser;

import java.util.Hashtable;
import myUtils.Dimension;
import myUtils.Triplet;

public class EstimateTable {
	//Case ISO (2D only): 	line -> "ancID_ancID_ancID_[0|1]" -> triplet
	//Case Chan-Ho: 		line -> "ancID" -> triplet
	//Case Wiki:			line -> "ancID_ancID_ancID_ancID" -> triplet
	Hashtable<Integer, Hashtable<String, Triplet>> table;
	String 	name;
	
	EstimateTable (String esttabname) {
		table = new Hashtable<Integer, Hashtable<String, Triplet>>();
		name = esttabname;
	}
	
	void put(int line, String str, Triplet tp) {
		if (!table.containsKey(line)) {
			table.put(line, new Hashtable<String, Triplet>());
		}
		
		table.get(line).put(str, tp);
	}
	
	void putISOEstimates(int line, String str, ISOEstimates est) {
		if (!table.containsKey(line) && est.getNumber() > 0) {
			table.put(line, new Hashtable<String, Triplet>());
		}
		
		for (int nn=0; nn<est.getNumber(); nn++) {
			table.get(line).put(str+"_"+nn, est.getTriplet(nn));
		}
	}
	
	Triplet get (int line, String str) {
		return table.get(line).get(str);
	}
	
	Hashtable<String, Triplet> get (int line) {
		return table.get(line);
	}
	
	boolean containsKey (int line) {
		return table.containsKey(line);
	}
	
	boolean containsKey (int line, String str) {
		return table.containsKey(line) ? table.get(line).containsKey(str) : false;
	}
	
//	String printISOCSV (int line, String ancindex, Dimension d) {
//		//line == 0 means printing headers
//		String str = "";
//		
//		if (line != 0) {
//			Hashtable<String, Triplet> ttable = table.get(line);
//			
//			if (ttable.containsKey(ancindex + "_0")) {
//				str += ttable.get(ancindex + "_0").printCSV(d);
//			}
//			else {
//				str += (d == Dimension.THREED) ? ", , , " : ", , ";
//			}
//			
//			if (ttable.containsKey(ancindex + "_1")) {
//				str += ttable.get(ancindex + "_1").printCSV(d); 
//			} 
//			else {
//				str += (d == Dimension.THREED) ? ", , , " : ", , ";
//			}
//		}
//		else {
//			String ext = "_" + name + "_" + ancindex + ",";
//			str += "x0_" + ext + "y0_" + ext + "x1_" + ext + "y1_" + ext;			
//		}
//		
//		return str;
//	}
	
	
	String printCSV (int line, String ancindex, Dimension d) {
		//print one coordinate only, so for ISO, this function must be called twice
		String str = "";
		
		if (line != 0) {
			//print contents (one coordinate)
			Hashtable<String, Triplet> ttable = table.get(line);
			
			if (ttable != null && ttable.containsKey(ancindex)) {
				str += ttable.get(ancindex).printCSV(d);
			}
			else {
				str += (d == Dimension.THREED) ? ",,," : ",,";
			}
		}
		else {
			//print headers
			String ext = "_" + name + "_" + ancindex + ",";
			str += "x" + ext + "y" + ext + (d == Dimension.THREED ? "z" + ext : "");
		}
		
		return str;
	}
	
//	String printCSVHeaders(Dimension d) {
//		String ext = "_" + name + (d == Dimension.THREED ? "_3D," : "_2D,");
//		
//		return "x" + ext + "y" + ext + (d == Dimension.THREED ? "z" + ext : "" );
//	}
//	
//	String printCSVHeaders(String suff, Dimension d) {
//		String ext = "_" + name + "_" + suff + ",";
//		
//		return "x" + ext + "y" + ext + (d == Dimension.THREED ? "z" + ext : "" );
//	}
}