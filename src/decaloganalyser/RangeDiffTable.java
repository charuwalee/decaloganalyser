package decaloganalyser;

import java.util.Hashtable;

public class RangeDiffTable {
	//Range difference:		
	//		line -> "ancID_ancID" -> rangediff
	//
	//ISO range difference error:
	//		line -> "ancID_ancID_ancID_[0|1]_ancID_ancID" -> rangedifferr
	//
	//Chan-Ho range difference error:
	//		line -> "ancID_ancID_ancID" -> rangedifferr
	Hashtable<Integer, Hashtable<String, Double>> table;
	String 	name;
														 
	
	RangeDiffTable (String tablename) {
		table = new Hashtable<Integer, Hashtable<String, Double>>();
		name = tablename; 
	}
	
	
	void put (int line, String str, double val) {
		if (!table.containsKey(line)) {
			table.put(line, new Hashtable<String, Double>());
		}
		
		table.get(line).put(str, val);
	}
	
	
	double get (int line, String str) {
		return table.get(line).get(str);
	}
	
	
	Hashtable<String, Double> get (int line) {
		return table.get(line);
	}
	
	
	boolean containsKey (int line, String str) {
		return table.containsKey(line) && table.get(line).containsKey(str);
	}
	
	
	boolean containsKey (int line) {
		return table.containsKey(line);
	}
	
	
	String printCSV (int line, String ancindex) {
		String str = "";
		
		if (line != 0) {
			//print contents
			if (containsKey(line, ancindex)) {
				str += get(line, ancindex);
			}
			str += ",";
		}
		else {
			//print headers
			str += name + "_" + ancindex + ",";
		}
		
		return str;
	}
}
