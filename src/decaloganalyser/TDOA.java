package decaloganalyser;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import Jama.Matrix;
import myUtils.Dimension;
import myUtils.Triplet;

public class TDOA {
	
	static final double		RANGE_NEAR_ZERO_IN_MM				= 0.001;
	static final int		MATRIX_PRINT_COLUMN_WIDTH			= 12;
	static final int		MATRIX_PRINT_DIGITS_AFTER_DECIMAL	= 6;

	public enum Variant {
		ISO, 
		CHANHO, CHANHO_2D, CHANHO_3D, 
		CHANHO_OLS, CHANHO_OLS_2D, CHANHO_OLS_3D, 
		CHANHO_QTLS, CHANHO_QTLS_2D, CHANHO_QTLS_3D,
		CHANHO_QTQPLS, CHANHO_QTQPLS_2D, CHANHO_QTQPLS_3D,
		CHANHOEXT, CHANHOEXT_2D, CHANHOEXT_3D,
		CHANHOEXT_OLS, CHANHOEXT_OLS_2D, CHANHOEXT_OLS_3D,
		CHANHOEXT_QTLS, CHANHOEXT_QTLS_2D, CHANHOEXT_QTLS_3D,
		WIKI, WIKI_2D, WIKI_3D,
		ISO_RA, ISO_NRA, ISO_AA,					
		CHANHO_OLS_RA_2D,  	 CHANHO_OLS_RA_3D, 		//Chan-Ho + OLS (no iteration) + related anchor selecting method
		CHANHO_OLS_NRA_2D, 	 CHANHO_OLS_NRA_3D,		//Chan-Ho + OLS (no iteration) + non-related anchor selecting method
		CHANHO_OLS_AA_2D,    CHANHO_OLS_AA_3D, 		//Chan-Ho + OLS (no iteration) + all anchor selecting method
		CHANHO_QTLS_RA_2D,   CHANHO_QTLS_RA_3D, 	//Chan-Ho + QTLS (consider process (or timestamp reading) noise) + related anchor selecting method
		CHANHO_QTLS_NRA_2D,  CHANHO_QTLS_NRA_3D,    //Chan-Ho + QTLS (consider process (or timestamp reading) noise) + non-related anchor selecting method
		CHANHO_QTLS_AA_2D,   CHANHO_QTLS_AA_3D,		//Chan-Ho + QTLS (consider process (or timestamp reading) noise) + all anchor selecting method
		CHANHO_QTQPLS_RA_2D, CHANHO_QTQPLS_RA_3D,	//Chan-Ho + QPTLS (consider process and position noise) + related anchor selecting method
		CHANHO_QTQPLS_NRA_2D,CHANHO_QTQPLS_NRA_3D,	//Chan-Ho + QPTLS (consider process and position noise) + non-related anchor selecting method
		CHANHO_QTQPLS_AA_2D, CHANHO_QTQPLS_AA_3D,	//Chan-Ho + QPTLS (consider process and position noise) + all anchor selecting method
		CHANHOEXT_QTLS_RA_2D,CHANHOEXT_QTLS_RA_3D,
		CHANHOEXT_QTLS_NRA_2D,CHANHOEXT_QTLS_NRA_3D,
		CHANHOEXT_QTLS_AA_2D,CHANHOEXT_QTLS_AA_3D,
		WIKI_RA_2D,  WIKI_RA_3D,					//Wiki + related anchor selecting method
		WIKI_NRA_2D, WIKI_NRA_3D,					//Wiki + non-related anchor selecting method
		WIKI_AA_2D,  WIKI_AA_3D						//Wiki + all anchor selecting method
	}

	public static String toString (Variant alg) {
		String str = "";
		switch (alg) {
		case ISO : str = "ISO"; break;
		case CHANHO: str = "Chan-Ho"; break;
		case CHANHO_2D: str = "Chan-Ho_2D"; break;
		case CHANHO_3D: str = "Chan-Ho_3D"; break;
		case CHANHO_OLS: str = "Chan-Ho_OLS"; break;
		case CHANHO_OLS_2D: str = "Chan-Ho_OLS_2D"; break;
		case CHANHO_OLS_3D: str = "Chan-Ho_OLS_3D"; break;
		case CHANHO_QTLS: str = "Chan-Ho_QTLS"; break;
		case CHANHO_QTLS_2D: str = "Chan-Ho_QTLS_2D"; break;
		case CHANHO_QTLS_3D: str = "Chan-Ho_QTLS_3D"; break;
		case CHANHO_QTQPLS: str = "Chan-Ho_QTQPLS"; break;
		case CHANHO_QTQPLS_2D: str = "Chan-Ho_QTQPLS_2D"; break;
		case CHANHO_QTQPLS_3D: str = "Chan-Ho_QTQPLS_3D"; break;
		case CHANHOEXT: str = "Chan-Ho-Ext"; break;
		case CHANHOEXT_2D: str = "Chan-Ho-Ext_2D"; break;
		case CHANHOEXT_3D: str = "Chan-Ho-Ext_3D"; break;
		case CHANHOEXT_OLS: str = "Chan-Ho-Ext_OLS"; break;
		case CHANHOEXT_OLS_2D: str = "Chan-Ho-Ext_OLS_2D"; break;
		case CHANHOEXT_OLS_3D: str = "Chan-Ho-Ext_OLS_3D"; break;
		case CHANHOEXT_QTLS: str = "Chan-Ho-Ext_QTLS"; break;
		case CHANHOEXT_QTLS_2D: str = "Chan-Ho-Ext_QTLS_2D"; break;
		case CHANHOEXT_QTLS_3D: str = "Chan-Ho-Ext_QTLS_3D"; break;
		case WIKI: str = "Wiki"; break;
		case WIKI_2D: str = "Wiki_2D"; break;
		case WIKI_3D: str = "Wiki_3D"; break;
		case ISO_RA: str = "ISO_RA"; break;
		case ISO_NRA: str = "ISO_NRA"; break;
		case ISO_AA: str = "ISO_AA"; break;
		case CHANHO_OLS_RA_2D: str = "Chan-Ho_OLS_RA_2D" ; break;
		case CHANHO_OLS_RA_3D: str = "Chan-Ho_OLS_RA_3D" ; break;
		case CHANHO_OLS_NRA_2D: str = "Chan-Ho_OLS_NRA_2D" ; break;
		case CHANHO_OLS_NRA_3D: str = "Chan-Ho_OLS_NRA_3D" ; break;
		case CHANHO_OLS_AA_2D: str = "Chan-Ho_OLS_AA_2D" ; break;
		case CHANHO_OLS_AA_3D: str = "Chan-Ho_OLS_AA_3D" ; break;
		case CHANHO_QTLS_RA_2D: str = "Chan-Ho_QTLS_RA_2D" ; break;
		case CHANHO_QTLS_RA_3D: str = "Chan-Ho_QTLS_RA_3D" ; break;
		case CHANHO_QTLS_NRA_2D: str = "Chan-Ho_QTLS_NRA_2D" ; break;
		case CHANHO_QTLS_NRA_3D: str = "Chan-Ho_QTLS_NRA_3D" ; break;
		case CHANHO_QTLS_AA_2D: str = "Chan-Ho_QTLS_AA_2D" ; break;
		case CHANHO_QTLS_AA_3D: str = "Chan-Ho_QTLS_AA_3D" ; break;
		case CHANHO_QTQPLS_RA_2D: str = "Chan-Ho_QTQPLS_RA_2D" ; break;
		case CHANHO_QTQPLS_RA_3D: str = "Chan-Ho_QTQPLS_RA_3D" ; break;
		case CHANHO_QTQPLS_NRA_2D: str = "Chan-Ho_QTQPLS_NRA_2D" ; break;
		case CHANHO_QTQPLS_NRA_3D: str = "Chan-Ho_QTQPLS_NRA_3D" ; break;
		case CHANHO_QTQPLS_AA_2D: str = "Chan-Ho_QTQPLS_AA_2D" ; break;
		case CHANHO_QTQPLS_AA_3D: str = "Chan-Ho_QTQPLS_AA_3D" ; break;
		case CHANHOEXT_QTLS_RA_2D: str = "Chan-Ho-Ext_QTLS_RA_2D"; break;
		case CHANHOEXT_QTLS_RA_3D: str = "Chan-Ho-Ext_QTLS_RA_3D"; break;
		case CHANHOEXT_QTLS_NRA_2D: str = "Chan-Ho-Ext_QTLS_NRA_2D"; break;
		case CHANHOEXT_QTLS_NRA_3D: str = "Chan-Ho-Ext_QTLS_NRA_3D"; break;
		case CHANHOEXT_QTLS_AA_2D: str = "Chan-Ho-Ext_QTLS_AA_2D"; break;
		case CHANHOEXT_QTLS_AA_3D: str = "Chan-Ho-Ext_QTLS_AA_3D"; break;
		case WIKI_RA_2D: str = "Wiki_RA_2D" ; break;
		case WIKI_RA_3D: str = "Wiki_RA_3D" ; break;
		case WIKI_NRA_2D: str = "Wiki_NRA_2D" ; break;
		case WIKI_NRA_3D: str = "Wiki_NRA_3D" ; break;
		case WIKI_AA_2D: str = "Wiki_AA_2D" ; break;
		case WIKI_AA_3D: str = "Wiki_AA_3D" ; break;
		}
		
		return str;
	}
	
	
	static ISOEstimates computeISOEstimates (double rangediffij, double rangediffik, double rangediffjk, Triplet anci, Triplet ancj, Triplet anck) {
		//compute lambda_ij
		double xi = anci.getX();
		double yi = anci.getY();
		double xj = ancj.getX();
		double yj = ancj.getY();
		double xk = anck.getX();
		double yk = anck.getY();
		double lambdaij = 0.5*( Math.pow(rangediffij, 2) + Math.pow(xi, 2) + Math.pow(yi, 2) - Math.pow(xj, 2) - Math.pow(yj, 2));
		double lambdaik = 0.5*( Math.pow(rangediffik, 2) + Math.pow(xi, 2) + Math.pow(yi, 2) - Math.pow(xk, 2) - Math.pow(yk, 2));
		double divisor = rangediffik*(yj-yi) - rangediffij*(yk-yi);
		double m = -((rangediffik*(xj-xi) - rangediffij*(xk-xi)) / divisor);
		double b = -((rangediffik*lambdaij - rangediffij*lambdaik) / divisor);
		double A = Math.pow(xj-xi,2) + Math.pow(m*(yj-yi), 2) + 2*m*(xj-xi)*(yj-yi) - Math.pow(rangediffij, 2) - Math.pow(m*rangediffij,2);
		double B = 2*(m*b*Math.pow(yj-yi, 2) + lambdaij*(xj-xi) + m*lambdaij*(yj-yi) + b*(xj-xi)*(yj-yi) + xi*Math.pow(rangediffij,2) - m*(b-yi)*Math.pow(rangediffij, 2));
		double C = Math.pow(lambdaij, 2) + Math.pow(b*(yj-yi), 2) + 2*b*lambdaij*(yj-yi) - Math.pow(xi*rangediffij, 2) - Math.pow(rangediffij*(b-yi), 2);
		double B2_4AC = Math.pow(B, 2) - 4*A*C;
		ISOEstimates est = new ISOEstimates();
		if (B2_4AC < 0) {
			return est;
		}
		double sqrtB24AC = Math.sqrt(B2_4AC);
		double x0 = (-B + sqrtB24AC)/(2*A);
		double x1 = (-B - sqrtB24AC)/(2*A);
		double y0 = m*x0+b;
		double y1 = m*x1+b;
		
		//check validity of candidate estimates
		double rangediffcalij = Math.sqrt(Math.pow(x0-xi,2) + Math.pow(y0-yi, 2)) - Math.sqrt(Math.pow(x0-xj,2) + Math.pow(y0-yj, 2));
		double rangediffcalik = Math.sqrt(Math.pow(x0-xi,2) + Math.pow(y0-yi, 2)) - Math.sqrt(Math.pow(x0-xk,2) + Math.pow(y0-yk, 2));
		double rangediffcaljk = Math.sqrt(Math.pow(x0-xj,2) + Math.pow(y0-yj, 2)) - Math.sqrt(Math.pow(x0-xk,2) + Math.pow(y0-yk, 2));
		if (Math.abs(rangediffij - rangediffcalij) <= RANGE_NEAR_ZERO_IN_MM && 
			Math.abs(rangediffik - rangediffcalik) <= RANGE_NEAR_ZERO_IN_MM && 
			Math.abs(rangediffjk - rangediffcaljk) <= RANGE_NEAR_ZERO_IN_MM) {
			est.addEstimate(new Triplet(x0, y0, 0)); //ISO considers only 2D measurements
		}
		
		rangediffcalij = Math.sqrt(Math.pow(x1-xi,2) + Math.pow(y1-yi, 2)) - Math.sqrt(Math.pow(x1-xj,2) + Math.pow(y1-yj, 2));
		rangediffcalik = Math.sqrt(Math.pow(x1-xi,2) + Math.pow(y1-yi, 2)) - Math.sqrt(Math.pow(x1-xk,2) + Math.pow(y1-yk, 2));
		rangediffcaljk = Math.sqrt(Math.pow(x1-xj,2) + Math.pow(y1-yj, 2)) - Math.sqrt(Math.pow(x1-xk,2) + Math.pow(y1-yk, 2));
		if (Math.abs(rangediffij - rangediffcalij) <= RANGE_NEAR_ZERO_IN_MM && 
			Math.abs(rangediffik - rangediffcalik) <= RANGE_NEAR_ZERO_IN_MM &&
			Math.abs(rangediffjk - rangediffcaljk) <= RANGE_NEAR_ZERO_IN_MM) {
			est.addEstimate(new Triplet(x1, y1, 0)); //ISO considers only 2D measurements
		}
		
		return est;
	}


	static Triplet computeAnchorBasedCHOLSEstimate (Hashtable<String, Double> rangediffsubtable, List<Triplet> anclist, int i, Dimension dimension) {
		// i is an index of the base anchor for calculation
		Triplet sol;
		int ancnum = anclist.size();
		Matrix h = new Matrix (ancnum-1, 1);
		Matrix G = new Matrix (ancnum-1, dimension == Dimension.TWOD ? 3 : 4);
		
		int    rowindex = 0;
		
		for (int j=0; j<ancnum; j++) {
			if (i != j) {
				Triplet ti = anclist.get(i);
				Triplet tj = anclist.get(j);
				Triplet tjminusti = tj.minus(ti);
				double xjminusxi = tjminusti.getX();
				double yjminusyi = tjminusti.getY();
				double zjminuszi = tjminusti.getZ();
				double rangediffji = rangediffsubtable.get(j+"_"+i);
				double hvalue = dimension == Dimension.TWOD ? Math.pow(rangediffji, 2) - tj.sumOfSquared(Dimension.TWOD) + ti.sumOfSquared(Dimension.TWOD) :
															  Math.pow(rangediffji, 2) - tj.sumOfSquared(Dimension.THREED) + ti.sumOfSquared(Dimension.THREED) ;
				
				h.set(rowindex, 0, hvalue);
				G.set(rowindex, 0, xjminusxi);
				G.set(rowindex, 1, yjminusyi);
				if (dimension == Dimension.TWOD) {
					G.set(rowindex, 2, rangediffji);
				}
				else {
					G.set(rowindex, 2, zjminuszi);
					G.set(rowindex, 3, rangediffji);
				}
				
				rowindex++;
			}//if(i!=j)
		}//for(int j)
		
		G = G.times(-2);
		
		// compute the LSE
		Matrix B = (((G.transpose().times(G)).inverse()).times(G.transpose())).times(h);
		
		if (dimension == Dimension.TWOD) {
			sol = new Triplet(B.get(0, 0), B.get(1, 0));
		}
		else {
			sol = new Triplet(B.get(0, 0), B.get(1, 0), B.get(2, 0));
		}
		
		// compute simplified version of LSE
		// Xb = y
		// (X_transpose)X(b*) = (X_transpose)y ;  <==== solve this equation to get b*
		//B = X.transpose().times(X).solve(X.transpose().times(Y));
		//if (dimension == Dimension.TWOD) {
		//	sol.add(new Triplet(B.get(0, 0), B.get(1, 0)));
		//}
		//else {
		//	sol.add(new Triplet(B.get(0, 0), B.get(1, 0), B.get(2, 0)));
		//}
		
		return sol;
	}
			
	
	
	//Fixed number of input anchors to 4 (indexed as 0, 1, 2, and 3). 
	static Triplet[] computeSystemWideCHQTLSEstimate (PrintWriter dbgout, double[][] rangediff, Triplet[] anclist, Dimension dimension) throws Exception {
		int ancnum = anclist.length;
		boolean is2dim = dimension == Dimension.TWOD;
		int varnum = is2dim ? 5 : 6; //number of variables to be solved
		int dimnum = is2dim ? 2 : 3; //number of required dimension/coordination of the tag position
		double[] x = new double[ancnum];
		double[] y = new double[ancnum];
		double[] z = new double[ancnum];
		double[] sumOfSquared = new double[ancnum];
		Matrix[] s = new Matrix[ancnum];
		Triplet[] t_est  = new Triplet[ancnum+1];  //**** THE LAST ELEMENT OF THIS ARRAY IS ALWAYS THE u1. ****//
		
		//init elementary values
		for (int i=0; i<ancnum; i++) {
			Triplet ti = anclist[i];
			x[i] = ti.getX();
			y[i] = ti.getY();
			z[i] = ti.getZ();
			sumOfSquared[i] = ti.sumOfSquared(Dimension.THREED);
			s[i] = new Matrix(is2dim ? new double[] {x[i], y[i]} : new double[] {x[i], y[i], z[i]}, is2dim ? 2 : 3);
		}
		
		
		//init h1
		double[][] h1values = { {Math.pow(rangediff[1][0],2) - sumOfSquared[1] + sumOfSquared[0]},
							 	{Math.pow(rangediff[2][0],2) - sumOfSquared[2] + sumOfSquared[0]},
							 	{Math.pow(rangediff[3][0],2) - sumOfSquared[3] + sumOfSquared[0]},
							 	{Math.pow(rangediff[2][1],2) - sumOfSquared[2] + sumOfSquared[1]},
							 	{Math.pow(rangediff[3][1],2) - sumOfSquared[3] + sumOfSquared[1]},
							 	{Math.pow(rangediff[3][2],2) - sumOfSquared[3] + sumOfSquared[2]}
							  };
		Matrix h1 = new Matrix (h1values);
		dbgout.println("Init h1:");
		h1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		//init G1
		double[][] g1values2d = { {x[1]-x[0], y[1]-y[0], rangediff[1][0], 0, 0},
								  {x[2]-x[0], y[2]-y[0], rangediff[2][0], 0, 0},
								  {x[3]-x[0], y[3]-y[0], rangediff[3][0], 0, 0},
								  {x[2]-x[1], y[2]-y[1], 0, rangediff[2][1], 0},
								  {x[3]-x[1], y[3]-y[1], 0, rangediff[3][1], 0},
								  {x[3]-x[2], y[3]-y[2], 0, 0, rangediff[3][2]}
								};
		double[][] g1values3d = { {x[1]-x[0], y[1]-y[0], z[1]-z[0], rangediff[1][0], 0, 0},
								  {x[2]-x[0], y[2]-y[0], z[2]-z[0], rangediff[2][0], 0, 0},
								  {x[3]-x[0], y[3]-y[0], z[3]-z[0], rangediff[3][0], 0, 0},
								  {x[2]-x[1], y[2]-y[1], z[2]-z[1], 0, rangediff[2][1], 0},
								  {x[3]-x[1], y[3]-y[1], z[3]-z[1], 0, rangediff[3][1], 0},
								  {x[3]-x[2], y[3]-y[2], z[3]-z[2], 0, 0, rangediff[3][2]}
			 					};
		Matrix G1 = new Matrix (is2dim ? g1values2d : g1values3d, 6, varnum);
		G1 = G1.times(-2);
		dbgout.println("Init G1:");
		G1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//init Qt
		//By definition Qt = (c^2)*(\delta t)*((\delta t).transpose())
		int n = -60; 	//n = -60:5:-5
		double power_n = Math.pow(10, n/10);
		Matrix Qt = matrixEye(6).times(power_n/2).plus(matrixOnes(6).times(power_n/2)) ;
		dbgout.println("Init Qt:");
		Qt.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//init W1 = inv(Qt)
		Matrix W1 = Qt.inverse();
		dbgout.println("Init W1 = inv(Qt):");
		W1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//compute first value of u1 = inv(G1'*W1*G1)*G1'*W1*h1
		Matrix u1 = null;
		
		try {
			u1 = ((G1.transpose().times(W1).times(G1)).inverse()).times(G1.transpose()).times(W1).times(h1);
		} catch (Exception e) {
			dbgout.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot compute the first value of u1. Exception received.");
			System.out.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot compute the first value of u1. Exception received.");
			for (int k=0; k < t_est.length; k++) {
				t_est[k] = null;
			}
			return t_est;
		}
		dbgout.println("Compute first u1 = inv(G1'*W1*G1)*G1'*W1*h1:");
		u1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		final int firststageloopnum = 1;
		double u1x, u1y, u1z;
		double[] ri = new double[4];
		Matrix B1 = null;
		
		for (int m=0; m < firststageloopnum; m++) {
			//init B1 by values from u1
			u1x = u1.get(0, 0);
			u1y = u1.get(1, 0);
			u1z = is2dim ? 0 : u1.get(2, 0);
			ri[0] = is2dim ? Math.sqrt(Math.pow(u1x-x[0], 2)+Math.pow(u1y-y[0], 2)) : Math.sqrt(Math.pow(u1x-x[0], 2)+Math.pow(u1y-y[0], 2)+Math.pow(u1z-z[0], 2));
			ri[1] = is2dim ? Math.sqrt(Math.pow(u1x-x[1], 2)+Math.pow(u1y-y[1], 2)) : Math.sqrt(Math.pow(u1x-x[1], 2)+Math.pow(u1y-y[1], 2)+Math.pow(u1z-z[1], 2));
			ri[2] = is2dim ? Math.sqrt(Math.pow(u1x-x[2], 2)+Math.pow(u1y-y[2], 2)) : Math.sqrt(Math.pow(u1x-x[2], 2)+Math.pow(u1y-y[2], 2)+Math.pow(u1z-z[2], 2));
			ri[3] = is2dim ? Math.sqrt(Math.pow(u1x-x[3], 2)+Math.pow(u1y-y[3], 2)) : Math.sqrt(Math.pow(u1x-x[3], 2)+Math.pow(u1y-y[3], 2)+Math.pow(u1z-z[3], 2));
			double[] b1values = { ri[1], ri[2], ri[3], ri[2], ri[3], ri[3] };
			B1 = matrixDiag(b1values).times(2); 
			dbgout.println("Compute ri (1<=i<=M) from u1.x, u1.y and u1.z....put in B1 matrix.");
			dbgout.println("Init B1 = 2*diag(r2, r3, r4, r3, r4, r4):");
			B1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
			
			
			//update W1 = inv(B1*Qt*B1')
			try {
				W1 = (B1.times(Qt).times(B1.transpose())).inverse();
			} catch (Exception e) {
				dbgout.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot update W1. Exception received.");
				System.out.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot update W1. Exception received.");
				for (int k=0; k < t_est.length; k++) {
					t_est[k] = null;
				}
				return t_est;
			}
			dbgout.println("Update W1=inv(B1*Qt*B1'):");
			W1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
			
			//update u1 = inv(G1'*W1*G1)*G1'*W1*h1
			try {
				u1 = ((G1.transpose().times(W1).times(G1)).inverse()).times(G1.transpose()).times(W1).times(h1);
			} catch (Exception e) {
				dbgout.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot update u1. Exception received.");
				System.out.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot update u1. Exception received.");
				for (int k=0; k < t_est.length; k++) {
					t_est[k] = null;
				}
				return t_est;
			}
			dbgout.println("Update u1=inv(G1'*W1*G1)*G1'*W1*h1:");
			u1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		}
		
		u1x = u1.get(0, 0);
		u1y = u1.get(1, 0);
		u1z = is2dim ? 0 : u1.get(2, 0); 
		//u1ri values are required to initialise B2i and h2i for the first time.
		double[] u1ri = { 	is2dim ? u1.get(2, 0) : u1.get(3, 0),  	 //u1ri[0] = r1 (anchor indices are 1,...,M)
							is2dim ? u1.get(3, 0) : u1.get(4, 0),	 //u1ri[1] = r2
							is2dim ? u1.get(4, 0) : u1.get(5, 0)  }; //u1ri[2] = r3
		
		t_est[ancnum] = new Triplet(u1x, u1y, u1z); //**** THE LAST ELEMENT OF THIS ARRAY IS ALWAYS THE u1. ****//
		
		
		//prepare for second stage
		//int stage2dim = dimnum+1;
		//update G1 (update rij values inside) by values from u1
		ri[0] = is2dim ? Math.sqrt(Math.pow(u1x-x[0], 2)+Math.pow(u1y-y[0], 2)) : Math.sqrt(Math.pow(u1x-x[0], 2)+Math.pow(u1y-y[0], 2)+Math.pow(u1z-z[0], 2));
		ri[1] = is2dim ? Math.sqrt(Math.pow(u1x-x[1], 2)+Math.pow(u1y-y[1], 2)) : Math.sqrt(Math.pow(u1x-x[1], 2)+Math.pow(u1y-y[1], 2)+Math.pow(u1z-z[1], 2));
		ri[2] = is2dim ? Math.sqrt(Math.pow(u1x-x[2], 2)+Math.pow(u1y-y[2], 2)) : Math.sqrt(Math.pow(u1x-x[2], 2)+Math.pow(u1y-y[2], 2)+Math.pow(u1z-z[2], 2));
		ri[3] = is2dim ? Math.sqrt(Math.pow(u1x-x[3], 2)+Math.pow(u1y-y[3], 2)) : Math.sqrt(Math.pow(u1x-x[3], 2)+Math.pow(u1y-y[3], 2)+Math.pow(u1z-z[3], 2));
		G1.set(0, is2dim ? 2 : 3, -2*(ri[1]-ri[0]));
		G1.set(1, is2dim ? 2 : 3, -2*(ri[2]-ri[0]));
		G1.set(2, is2dim ? 2 : 3, -2*(ri[3]-ri[0]));
		G1.set(3, is2dim ? 3 : 4, -2*(ri[2]-ri[1]));
		G1.set(4, is2dim ? 3 : 4, -2*(ri[3]-ri[1]));
		G1.set(5, is2dim ? 4 : 5, -2*(ri[3]-ri[2]));
		dbgout.println("Update G1:");
		G1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//init G2
		double[][] g2val2d = { {1,0}, {0,1}, {1,1} };
		double[][] g2val3d = { {1,0,0}, {0,1,0}, {0,0,1}, {1,1,1} };
		Matrix G2 = new Matrix(is2dim ? g2val2d : g2val3d);
		dbgout.println("Init G2:");
		G2.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//prepare cov(u1i) values
		Matrix Covu1 = null;
		try {
			Covu1 = (G1.transpose().times(W1).times(G1)).inverse();
		} catch (Exception e) {
			dbgout.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot compute Covu1. Exception received.");
			System.out.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot compute Covu1. Exception received.");
			for (int k=0; k < t_est.length-1; k++) {
				t_est[k] = null;
			}
			return t_est;
		}
		
		dbgout.println("Compute cov(u1):");
		Covu1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		Matrix[] Covu1ri = new Matrix[ancnum-1];
		for (int i=0; i<ancnum-1; i++) {
			Covu1ri[i] = new Matrix(dimnum+1, dimnum+1);
			for (int r=0; r<dimnum+1; r++) {
				for (int c=0; c<dimnum+1; c++) {
					int rindex = (r<dimnum) ? r : i+dimnum;
					int cindex = (c<dimnum) ? c : i+dimnum;
					Covu1ri[i].set(r, c, Covu1.get(rindex, cindex));
				}
			}
			dbgout.println("Compute cov(u1r" + Integer.toString(i+1) + "):");
			Covu1ri[i].print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		}		
		
		
		/**** THE CONDITION i<ancnum-1 STEMS FROM THE FACT THAT 
		 * u1ri EXCLUDES THE r4 VALUE AND THEREFORE THE NUMBER OF CANDIDATES RESULTED FROM
		 * STAGE TWO WILL BE ONLY 3 (FROM r1, r2 AND r3 RESPECTIVELY)
		 ****/
		for (int i=0; i<ancnum-1; i++) {
			dbgout.println("==== Compute stage 2 for anchor " + Integer.toString(i+1) + ".");
			
			//init B2i, W2i and h2i (using u1ri values for initialisation)
			double[] b2ival2d = { 2*(u1x-x[i]), 2*(u1y-y[i]), 2*u1ri[i] };
			double[] b2ival3d = { 2*(u1x-x[i]), 2*(u1y-y[i]), 2*(u1z-z[i]), 2*u1ri[i] };
			double[][] h2ival2d = {  {Math.pow(u1x-x[i], 2)}, {Math.pow(u1y-y[i], 2)}, {Math.pow(u1ri[i], 2)} };
			double[][] h2ival3d = {  {Math.pow(u1x-x[i], 2)}, {Math.pow(u1y-y[i], 2)}, {Math.pow(u1z-z[i], 2)}, {Math.pow(u1ri[i], 2)} };
			Matrix B2i = matrixDiag(is2dim ? b2ival2d : b2ival3d);
			Matrix W2i = null;
			try {
				W2i = (B2i.times(Covu1ri[i]).times(B2i.transpose())).inverse();
			} catch (Exception e) {
				dbgout.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot compute W2i. Exception received.");
				System.out.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot compute W2i. Exception received.");
				t_est[i] = null;
				continue;
			}
			Matrix h2i = new Matrix(is2dim ? h2ival2d : h2ival3d);
			Matrix u2i = null;
			try {
				u2i = (G2.transpose().times(W2i).times(G2)).inverse().times(G2.transpose()).times(W2i).times(h2i);
			} catch (Exception e) {
				dbgout.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot compute u2i. Exception received.");
				System.out.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot compute u2i. Exception received.");
				t_est[i] = null;
				continue;
			}
			dbgout.println("Compute B2i (1<=i<=M):");
			B2i.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
			dbgout.println("Compute W2i (1<=i<=M):");
			W2i.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
			dbgout.println("Compute h2i (1<=i<=M):");
			h2i.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
			dbgout.println("Compute u2i (1<=i<=M):");
			u2i.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
			
			//must check if any element in u2i is a negative value. 
			if ( (u2i.get(0, 0) < 0 ) || (u2i.get(1, 0) < 0) || (!is2dim && u2i.get(2, 0) < 0) ) {
				t_est[i] = null;
				dbgout.println("u2i is **** unsolvable ****");
			}
			else {
				double[][] sign2d = { {Math.signum(u1x-x[i])}, {Math.signum(u1y-y[i])} };
				double[][] sign3d = { {Math.signum(u1x-x[i])}, {Math.signum(u1y-y[i])}, {Math.signum(u1z-z[i])} };
				Matrix sign = new Matrix (is2dim ? sign2d : sign3d);
				u2i = matrixSqrt(u2i).arrayTimes(sign).plus(s[i]);
				dbgout.println("tag position based on u2i:");
				u2i.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
			
				for (int j=0; j<3; j++) {
					dbgout.println("Loop refining " + Integer.toString(j+1) + " of 3 times:");		
					//update ri and ri1 (to be used in B1 and G1) by values from u2
					double u2x = u2i.get(0, 0);
					double u2y = u2i.get(1, 0);
					double u2z = is2dim ? 0 : u2i.get(2, 0);
					ri[0] = is2dim ? Math.sqrt(Math.pow(u2x-x[0], 2)+Math.pow(u2y-y[0], 2)) : Math.sqrt(Math.pow(u2x-x[0], 2)+Math.pow(u2y-y[0], 2)+Math.pow(u2z-z[0], 2));
					ri[1] = is2dim ? Math.sqrt(Math.pow(u2x-x[1], 2)+Math.pow(u2y-y[1], 2)) : Math.sqrt(Math.pow(u2x-x[1], 2)+Math.pow(u2y-y[1], 2)+Math.pow(u2z-z[1], 2));
					ri[2] = is2dim ? Math.sqrt(Math.pow(u2x-x[2], 2)+Math.pow(u2y-y[2], 2)) : Math.sqrt(Math.pow(u2x-x[2], 2)+Math.pow(u2y-y[2], 2)+Math.pow(u2z-z[2], 2));
					ri[3] =	is2dim ? Math.sqrt(Math.pow(u2x-x[3], 2)+Math.pow(u2y-y[3], 2)) : Math.sqrt(Math.pow(u2x-x[3], 2)+Math.pow(u2y-y[3], 2)+Math.pow(u2z-z[3], 2));
//					B1.set(0, 0, ri[1]*2);
//					B1.set(1, 1, ri[2]*2);
//					B1.set(2, 2, ri[3]*2);
//					B1.set(3, 3, ri[2]*2);
//					B1.set(4, 4, ri[3]*2);
//					B1.set(5, 5, ri[3]*2);
//					dbgout.println("Update B1:");
//					B1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
					G1.set(0, is2dim ? 2 : 3, -2*(ri[1]-ri[0]));
					G1.set(1, is2dim ? 2 : 3, -2*(ri[2]-ri[0]));
					G1.set(2, is2dim ? 2 : 3, -2*(ri[3]-ri[0]));
					G1.set(3, is2dim ? 3 : 4, -2*(ri[2]-ri[1]));
					G1.set(4, is2dim ? 3 : 4, -2*(ri[3]-ri[1]));
					G1.set(5, is2dim ? 4 : 5, -2*(ri[3]-ri[2]));
					dbgout.println("Update G1:");
					G1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
					
					
					//prepare in-loop cov(u1i) values
					Matrix Covu1_il = null;
					try {
						Covu1_il = (G1.transpose().times(W1).times(G1)).inverse();
					} catch (Exception e) {
						dbgout.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot compute Covu1_il. Exception received.");
						System.out.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot compute Covu1_il. Exception received.");
						t_est[i] = null;
						continue;
					}
					dbgout.println("Compute in-loop cov(u1):");
					Covu1_il.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
					Matrix Covu1ri_il = new Matrix(dimnum+1, dimnum+1);
					for (int r=0; r<dimnum+1; r++) {
						for (int c=0; c<dimnum+1; c++) {
							int rindex = (r<dimnum) ? r : i+dimnum;
							int cindex = (c<dimnum) ? c : i+dimnum;
							Covu1ri_il.set(r, c, Covu1_il.get(rindex, cindex));
						}
					}
					dbgout.println("Compute in-loop cov(u1r"+ Integer.toString(i+1) +"):");
					Covu1ri_il.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
					
					
					//update B2i, W2i and h2i
					B2i.set(0, 0, 2*(u2x-x[i]));
					B2i.set(1, 1, 2*(u2y-y[i]));
					if (is2dim) {
						B2i.set(2, 2, 2*ri[i]);
					} else { 
						B2i.set(2, 2,  2*(u2z-z[i]));
						B2i.set(3, 3, 2*ri[i]); 
					}
					try {
						W2i = (B2i.times(Covu1ri_il).times(B2i.transpose())).inverse();
					} catch (Exception e) {
						dbgout.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot update W2i. Exception received.");
						System.out.println("TDOA:computeSystemWideCHQTLSEstimate(): Cannot update W2i. Exception received.");
						t_est[i] = null;
						continue;
					}
					
					h2i.set(0, 0, Math.pow(u2x-x[i], 2));
					h2i.set(1, 0, Math.pow(u2y-y[i], 2));
					if (is2dim) {
						h2i.set(2, 0, Math.pow(ri[i], 2));
					} else {
						h2i.set(2, 0, Math.pow(u2z-z[i], 2));
						h2i.set(3, 0, Math.pow(ri[i], 2));
					}
					
					u2i = (G2.transpose().times(W2i).times(G2)).inverse().times(G2.transpose()).times(W2i).times(h2i);
					dbgout.println("Update B2i (1<=i<=M):");
					B2i.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
					dbgout.println("Update W2i (1<=i<=M):");
					W2i.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
					dbgout.println("Update h2i (1<=i<=M):");
					h2i.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
					dbgout.println("Compute new u2i (1<=i<=M):");
					u2i.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
					
					//must check if any element in u2i is a negative value. 
					if ( (u2i.get(0, 0) < 0 ) || (u2i.get(1, 0) < 0) || (!is2dim && u2i.get(2, 0) < 0) ) {
						dbgout.println("u2i is **** unsolvable ****");
						break;
					}
					else {
						u2i = matrixSqrt(u2i).arrayTimes(sign).plus(s[i]);
						dbgout.println("tag position based on u2i:");
						u2i.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
					}
				} //for (0<j<3)
			
				t_est[i] = new Triplet(u2i.get(0, 0), u2i.get(1, 0), is2dim ? 0 : u2i.get(2, 0));
			} //else
		} //for (0<i<ancnum-1)
		
		return t_est;  //**** THE LAST ELEMENT OF THIS ARRAY IS ALWAYS THE u1. ****//
	}
	
	
	
	static Triplet computeAnchorBasedCHQTLSEstimate (Hashtable<String, Double> rangediffsubtable, List<Triplet> anclist, int i, Dimension dimension) throws Exception {
		// i is an index of the base anchor for calculation
		
		int ancnum = anclist.size();
		int dms = dimension == Dimension.TWOD ? 3 : 4;
		Matrix si = new Matrix (dms-1, ancnum);		
		Matrix sitsi = new Matrix (ancnum, 1);
		Matrix xi = new Matrix (1, ancnum);
		Matrix yi = new Matrix (1, ancnum);
		Matrix zi = new Matrix (1, ancnum);
		Matrix s1 = new Matrix (dms-1, 1);
		
		//construct xi, yi, zi, sitsi and static vectors
		Triplet t = anclist.get(i);
		sitsi.set(0, 0, t.sumOfSquared(dimension));
		si.set(0, 0, t.getX());
		si.set(1, 0, t.getY());
		if (dimension == Dimension.THREED) {
			si.set(2, 0, t.getZ());
		}
		int rowid = 1; 
		for (int ancid=0; ancid<ancnum; ancid++) {
			if (ancid != i) {
				t = anclist.get(ancid);
				sitsi.set(rowid, 0, t.sumOfSquared(dimension));
				si.set(0, rowid, t.getX());
				si.set(1, rowid, t.getY());
				if (dimension == Dimension.THREED) {
					si.set(2, rowid, t.getZ());
				}
				rowid++;
			}
		}
		
		xi = si.getMatrix(0, 0, 0, ancnum-1);
		yi = si.getMatrix(1, 1, 0, ancnum-1);
		if (dimension == Dimension.THREED) {
			zi = si.getMatrix(2, 2, 0, ancnum-1);
		}
		s1 = si.getMatrix(0, dms-2, 0, 0);
		
		Matrix G2 = new Matrix(dms, dms-1);
		G2.set(0, 0, 1.0);
		G2.set(1, 1, 1.0);
		G2.set(dms-1, 0, 1.0); //last row
		G2.set(dms-1, 1, 1.0); //last row
		if (dimension == Dimension.THREED) {
			G2.set(dms-2, dms-2, 1.0); //next to last row
			G2.set(dms-1, dms-2, 1.0); //last row & last column	
		}
		
		
		//construct ri1 from collected values
		Matrix ri1 = new Matrix (ancnum-1, 1);
		rowid=0;
		for (int j=0; j<ancnum; j++) {
			if (i!=j) {
				ri1.set(rowid++, 0, rangediffsubtable.get(j+"_"+i));
			}
		}
		
		
		//init Qt
		int n = -60; 	//n = -60:5:-5
		double power_n = Math.pow(10, n/10);
		Matrix Qt = matrixEye(ancnum-1).times(power_n/2).plus(matrixOnes(ancnum-1).times(power_n/2)) ;
		
		
		//init W1 = inv(Qt)
		Matrix W1 = Qt.inverse();
		
		
		//init G1 and h1 from collected values
		Matrix h1 = ri1.arrayTimes(ri1);
		h1.minusEquals(sitsi.getMatrix(1, ancnum-1, 0, 0));
		h1.plusEquals(new Matrix(ancnum-1, 1, sitsi.get(0, 0)));
		Matrix G1 = si.getMatrix(0, dms-2, 1, ancnum-1).minus(matrixHExtend (s1, ancnum-1)).transpose();
		G1 = matrixHConcate(G1, ri1);
		G1.timesEquals(-2);
		
		
		//compute first value of u1 = inv(G1'*W1*G1)*G1'*W1*h1
		Matrix T1 = ((G1.transpose().times(W1).times(G1)).inverse()).times(G1.transpose()).times(W1).times(h1);
		
		
		//init B1 by values from u1
		Matrix ri = matrixSquare(matrixMinus(xi.transpose(), T1.get(0, 0))).plus(matrixSquare(matrixMinus(yi.transpose(), T1.get(1, 0))));
		if (dimension == Dimension.THREED) {
			ri = ri.plus(matrixSquare(matrixMinus(zi.transpose(), T1.get(2, 0))));
		}
		ri = matrixSqrt(ri);
		Matrix B1 = matrixDiag(ri.getMatrix(1, ancnum-1, 0, 0)).times(2);
		
		
		//update W1 = inv(B1*Q1*B1')
		W1 = (B1.times(Qt).times(B1.transpose())).inverse();
		
		
		//update T1 = inv(G1'*W1*G1)*G1'*W1*h1
		T1 = ((G1.transpose().times(W1).times(G1)).inverse()).times(G1.transpose()).times(W1).times(h1);
		
		
		//prepare for second stage 
		//update G1 (update ri1 values inside) by values from u1
//		ri = matrixSquare(matrixMinus(xi.transpose(), T1.get(0, 0))).plus(matrixSquare(matrixMinus(yi.transpose(), T1.get(1, 0))));
//		if (dimension == Dimension.THREED) {
//			ri = ri.plus(matrixSquare(matrixMinus(zi.transpose(), T1.get(2, 0))));
//		}
//		ri = matrixSqrt(ri);
		ri1 = matrixMinus(ri.getMatrix(1, ancnum-1, 0, 0), ri.get(0, 0));
		G1.setMatrix(0, ancnum-2, dms-1, dms-1, ri1.times(-2));

		
		//init B2 by values from u1
		Matrix B2 = matrixDiag(T1.minus(matrixVConcate(s1, 0))).times(2);
		
		
		//init W2 = inv(B2')*(G1'*W1*G1)*inv(B2); \eq inv(B2*inv(G1'*W1*G1)*B2')
		Matrix W2 = (B2.times((G1.transpose().times(W1).times(G1)).inverse()).times(B2.transpose())).inverse();
		
		
		//init h2 by values from u1
		Matrix h2 = matrixSquare(T1.minus(matrixVConcate(s1, 0)));
		
		
		//compute first value of T2 = inv(G2'*W2*G2)*G2'*W2*h2
		Matrix T2 = ((G2.transpose().times(W2).times(G2)).inverse()).times(G2.transpose()).times(W2).times(h2);
		
		//must check if any element in T2 is a negative value. 
		if ( (T2.get(0, 0) < 0 ) || (T2.get(1, 0) < 0) || (dimension == Dimension.THREED && T2.get(2, 0) < 0) ){
			return null;
		}
		
		Matrix u2 = matrixSqrt(T2).arrayTimes(matrixSign(T1.getMatrix(0, dms-2, 0, 0).minus(s1))).plus(s1);
		
		
		//loop 3 times
		for (int j=0; j<3; j++) {
			//update ri and G1 by values from u2
			ri = matrixSquare(matrixMinus(xi.transpose(), u2.get(0, 0))).plus(matrixSquare(matrixMinus(yi.transpose(), u2.get(1, 0))));
			if (dimension == Dimension.THREED) {
				ri = ri.plus(matrixSquare(matrixMinus(zi.transpose(), u2.get(2, 0))));
			}
			ri = matrixSqrt(ri);
			ri1 = matrixMinus(ri.getMatrix(1, ancnum-1, 0, 0), ri.get(0, 0));
			G1.setMatrix(0, ancnum-2, dms-1, dms-1, ri1.times(-2));
			
			//update B2; W2 = inv(B2)*(G1'*W1*G1)*inv(B2); h2
			B2 = matrixDiag(matrixVConcate(u2, ri.get(0, 0)).minus(matrixVConcate(s1, 0))).times(2);
			W2 = (B2.times((G1.transpose().times(W1).times(G1)).inverse()).times(B2.transpose())).inverse();
			h2 = matrixSquare(matrixVConcate(u2, ri.get(0, 0)).minus(matrixVConcate(s1, 0)));
			
			
			//update T2 = inv(G2'*W2*G2)*G2'*W2*h2
			T2 = ((G2.transpose().times(W2).times(G2)).inverse()).times(G2.transpose()).times(W2).times(h2);
			//u2 = matrixSign(matrixSqrt(T2)).plus(s1);
			u2 = matrixSqrt(T2).arrayTimes(matrixSign(T1.getMatrix(0, dms-2, 0, 0).minus(s1))).plus(s1);
		}
		//end loop 3 times
	
		Triplet u = new Triplet(u2.get(0, 0), u2.get(1, 0));
		if (dimension == Dimension.THREED) {
			u.setValue("z", u2.get(2, 0));
		}
		return u;
	}
	
	
	
	static Triplet computeAnchorBasedCHQTLSEstimate (PrintWriter dbgout, double[][] rangediff, Triplet[] anclist, Dimension dimension) throws Exception {
		// Assume the base anchor is indexed 0.
		// Requires at least 5 anchors.
		
		int ancnum = anclist.length;
		boolean is2dim = dimension == Dimension.TWOD;
		int varnum = is2dim ? 3 : 4; //number of variables to be solved
		double[] x = new double[ancnum];
		double[] y = new double[ancnum];
		double[] z = new double[ancnum];
		double[] sumOfSquared = new double[ancnum];
		Matrix[] s = new Matrix[ancnum];
		
		dbgout.println("===========================");
		
		//init elementary values
		for (int i=0 ; i<ancnum; i++) {
			Triplet ti = anclist[i];
			x[i] = ti.getX();
			y[i] = ti.getY();
			z[i] = ti.getZ();
			sumOfSquared[i] = ti.sumOfSquared(Dimension.THREED);
			double[] svalue2d = {x[i], y[i]};
			double[] svalue3d = {x[i], y[i], z[i]};
			s[i] = new Matrix(is2dim ? svalue2d : svalue3d, is2dim ? 2 : 3);
		}
		
		
		//init h1
		Matrix h1 = new Matrix (ancnum-1, 1);
		for (int r=0; r<ancnum-1; r++) {
			h1.set(r, 1, Math.pow(rangediff[r+1][0], 2) - sumOfSquared[1] + sumOfSquared[0]);
		}
		dbgout.println("Init H1:");
		h1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//init G1
		Matrix G1 = new Matrix (ancnum-1, varnum);
		for (int r=0; r<ancnum-1; r++) {
			G1.set(r, 0, -2*(x[r+1]-x[0]));  
			G1.set(r, 1, -2*(y[r+1]-y[0]));  
			G1.set(r, 2, is2dim ? -2*(rangediff[r+1][0]) : -2*(z[r+1]-z[0]));
			if (!is2dim) {
				G1.set(r, 3, -2*(rangediff[r+1][0]));
			}
		}
		dbgout.println("Init G1:");
		G1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//init Qt
		//By definition Qt = (c^2)*(\delta t)*((\delta t).transpose())
		int n = -60; 	//n = -60:5:-5
		double power_n = Math.pow(10, n/10);
		Matrix Qt = matrixEye(ancnum-1).times(power_n/2).plus(matrixOnes(ancnum-1).times(power_n/2)) ;
		dbgout.println("Init Qt:");
		Qt.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//init W1 = inv(Qt)
		Matrix W1 = Qt.inverse();
		dbgout.println("Init W1 = inv(Qt):");
		W1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//compute first value of U1 = inv(G1'*W1*G1)*G1'*W1*H1
		Matrix u1 = ((G1.transpose().times(W1).times(G1)).inverse()).times(G1.transpose()).times(W1).times(h1);
		dbgout.println("Compute first U1 = inv(G1'*W1*G1)*G1'*W1*H1:");
		u1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);		
		
		
		//init B1 by values from U1
		double u1x = u1.get(0, 0);
		double u1y = u1.get(1, 0);
		double u1z = is2dim ? 0 : u1.get(2, 0);
		Matrix B1 = new Matrix(ancnum-1, ancnum-1);
		for (int r=0; r<ancnum-1; r++) {
			B1.set(r, r, is2dim ? 2*(Math.sqrt(Math.pow(u1x-x[r+1], 2)+Math.pow(u1y-y[r+1], 2))) : 2*(Math.sqrt(Math.pow(u1x-x[r+1], 2)+Math.pow(u1y-y[r+1], 2)+Math.pow(u1z-z[r+1], 2))));
		}
		dbgout.println("Init B1 = 2*diag(r2,..., rm):");
		B1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//update W1 = inv(B1*Q1*B1')
		W1 = (B1.times(Qt).times(B1.transpose())).inverse();
		dbgout.println("Update W1 = inv(B1QtB1'):");
		W1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//update u1 = inv(G1'*W1*G1)*G1'*W1*h1
		u1 = ((G1.transpose().times(W1).times(G1)).inverse()).times(G1.transpose()).times(W1).times(h1);
				
		
		//prepare for second stage 
		//update G1 (update ri1 values inside) by values from u1
		u1x = u1.get(0, 0);
		u1y = u1.get(1, 0);
		u1z = is2dim ? 0 : u1.get(2, 0);
		double r1_2d = Math.sqrt(Math.pow(u1x-x[0], 2)+Math.pow(u1y-y[0], 2));
		double r1_3d = Math.sqrt(Math.pow(u1x-x[0], 2)+Math.pow(u1y-y[0], 2)+Math.pow(u1z-z[0], 2));
		for (int r=0; r<ancnum-1; r++) {
			double ri = is2dim ? Math.sqrt(Math.pow(u1x-x[r+1],2)+Math.pow(u1y-y[r+1],2)) : Math.sqrt(Math.pow(u1x-x[r+1],2)+Math.pow(u1y-y[r+1],2)+Math.pow(u1z-z[r+1],2));
			G1.set(r, is2dim ? 2 : 3, is2dim ? -2*(ri-r1_2d) : -2*(ri-r1_3d));
		}
		dbgout.println("Update G1:");
		G1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//init B2 by values from u1
		Matrix B2 = matrixDiag(u1.minus(matrixVConcate(s[0], 0))).times(2);
		dbgout.println("Init B2:");
		B2.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
				
		
		//init W2 = inv(B2')*(G1'*W1*G1)*inv(B2); \eq inv(B2*inv(G1'*W1*G1)*B2')
		Matrix W2 = (B2.times((G1.transpose().times(W1).times(G1)).inverse()).times(B2.transpose())).inverse();
		dbgout.println("Init W2:");
		W2.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
				
		
		//init h2 by values from u1
		Matrix h2 = matrixSquare(u1.minus(matrixVConcate(s[0], 0)));
		dbgout.println("Init h2:");
		h2.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
				
		
		//init G2
		double[][] g2val2d = { {1,0}, {0,1}, {1,1} };
		double[][] g2val3d = { {1,0,0}, {0,1,0}, {0,0,1}, {1,1,1} };
		Matrix G2 = new Matrix(is2dim ? g2val2d : g2val3d);
		dbgout.println("Init G2:");
		G2.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
		
		
		//compute first value of u2 = inv(G2'*W2*G2)*G2'*W2*h2
		Matrix u2 = ((G2.transpose().times(W2).times(G2)).inverse()).times(G2.transpose()).times(W2).times(h2);
				
		
		//must check if any element in T2 is a negative value. 
		if ( (u2.get(0, 0) < 0 ) || (u2.get(1, 0) < 0) || (!is2dim && u2.get(2, 0) < 0) ) {
			return null;
		}
		Matrix usign = matrixSign(u1.getMatrix(0, varnum-2, 0, 0).minus(s[0]));
		u2 = matrixSqrt(u2).arrayTimes(usign).plus(s[0]);
		
		
		//loop 3 times
		for (int j=0; j<3; j++) {
			//update G1 (update ri1 values inside) by values from u2
			double u2x = u2.get(0, 0);
			double u2y = u2.get(1, 0);
			double u2z = is2dim ? 0 : u2.get(2, 0);
			r1_2d = Math.sqrt(Math.pow(u2x-x[0], 2)+Math.pow(u2y-y[0], 2));
			r1_3d = Math.sqrt(Math.pow(u2x-x[0], 2)+Math.pow(u2y-y[0], 2)+Math.pow(u2z-z[0], 2));
			for (int r=0; r<ancnum-1; r++) {
				double ri = is2dim ? Math.sqrt(Math.pow(u2x-x[r+1],2)+Math.pow(u2y-y[r+1],2)) : Math.sqrt(Math.pow(u2x-x[r+1],2)+Math.pow(u2y-y[r+1],2)+Math.pow(u2z-z[r+1],2));
				G1.set(r, is2dim ? 2 : 3, is2dim ? -2*(ri-r1_2d) : -2*(ri-r1_3d));
			}
			dbgout.println("Update G1:");
			G1.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
			
			
			//Update B2 by values from u2
			B2 = matrixDiag(matrixVConcate(u2, is2dim ? r1_2d : r1_3d).minus(matrixVConcate(s[0],0))).times(2);
			dbgout.println("Update B2:");
			B2.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
					
			
			//Update W2 = inv(B2')*(G1'*W1*G1)*inv(B2); \eq inv(B2*inv(G1'*W1*G1)*B2')
			W2 = (B2.times((G1.transpose().times(W1).times(G1)).inverse()).times(B2.transpose())).inverse();
			dbgout.println("Init W2:");
			W2.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
					
			
			//Update h2 by values from u2
			h2 = matrixSquare(matrixVConcate(u2, is2dim ? r1_2d : r1_3d).minus(matrixVConcate(s[0], 0)));
			dbgout.println("Update h2:");
			h2.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
			
			
			//update u2 = inv(G2'*W2*G2)*G2'*W2*h2
			u2 = ((G2.transpose().times(W2).times(G2)).inverse()).times(G2.transpose()).times(W2).times(h2);
			dbgout.println("Update u2:");
			u2.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);
			u2 = matrixSqrt(u2).arrayTimes(usign).plus(s[0]);
			dbgout.println("Square root u2:");
			u2.print(dbgout, MATRIX_PRINT_COLUMN_WIDTH, MATRIX_PRINT_DIGITS_AFTER_DECIMAL);			
		} //end loop 3 times
	
		
		return is2dim ? new Triplet(u2.get(0, 0), u2.get(1, 0)) : new Triplet(u2.get(0, 0), u2.get(1, 0), u2.get(2, 0));
	}
	
	
	
	static Triplet computeAnchorBasedCHQTQPLSCHEstimate (Hashtable<String, Double> rangediffsubtable, List<Triplet> anclist, int i, Dimension dimension) throws Exception {
		// i is an index of the base anchor for calculation
		
		int ancnum = anclist.size();
		int dms = dimension == Dimension.TWOD ? 3 : 4;
		Matrix si 	 = new Matrix (dms-1, ancnum);		
		Matrix sitsi = new Matrix (ancnum, 1);
		Matrix xi, yi, zi=null, ri=null;
		Matrix s1 = new Matrix (dms-1, 1);
		
		//construct xi, yi, zi, sitsi and static vectors
		Triplet t = anclist.get(i);
		sitsi.set(0, 0, t.sumOfSquared(dimension));
		si.set(0, 0, t.getX());
		si.set(1, 0, t.getY());
		if (dimension == Dimension.THREED) {
			si.set(2, 0, t.getZ());
		}
		int rowid = 1; 
		for (int ancid=0; ancid<ancnum; ancid++) {
			if (ancid != i) {
				t = anclist.get(ancid);
				sitsi.set(rowid, 0, t.sumOfSquared(dimension));
				si.set(0, rowid, t.getX());
				si.set(1, rowid, t.getY());
				if (dimension == Dimension.THREED) {
					si.set(2, rowid, t.getZ());
				}
				rowid++;
			}
		}
		
		xi = si.getMatrix(0, 0, 0, ancnum-1);
		yi = si.getMatrix(1, 1, 0, ancnum-1);
		if (dimension == Dimension.THREED) {
			zi = si.getMatrix(2, 2, 0, ancnum-1);
		}
		s1 = si.getMatrix(0, dms-2, 0, 0);
		
		
		Matrix G2 = new Matrix(dms, dms-1);
		G2.set(0, 0, 1.0);
		G2.set(1, 1, 1.0);
		G2.set(dms-1, 0, 1.0); //last row
		G2.set(dms-1, 1, 1.0); //last row
		if (dimension == Dimension.THREED) {
			G2.set(dms-2, dms-2, 1.0); //next to last row
			G2.set(dms-1, dms-2, 1.0); //last row & last column	
		}
		
		
		//construct ri1 from collected values
		Matrix ri1 = new Matrix (ancnum-1, 1);
		rowid=0;
		for (int j=0; j<ancnum; j++) {
			if (i!=j) {
				ri1.set(rowid++, 0, rangediffsubtable.get(j+"_"+i));
			}
		}
		
		
		//init G1 and h1 from collected values
		Matrix h1 = ri1.arrayTimes(ri1);
		h1.minusEquals(sitsi.getMatrix(1, ancnum-1, 0, 0));
		h1.plusEquals(new Matrix(ancnum-1, 1, sitsi.get(0, 0)));
		Matrix G1 = si.getMatrix(0, dms-2, 1, ancnum-1).minus(matrixHExtend (s1, ancnum-1)).transpose();
		G1 = matrixHConcate(G1, ri1);
		G1.timesEquals(-2);
		
		
		//init Qt, Qp, and W1=inv(Qt+Qp)
		int n = -60; 	//n = -60:5:-5
		double power_n 	= Math.pow(10,(n/10));
		double power_m 	= 5*Math.pow(10,-2);
		Matrix Qt 		= matrixEye(ancnum-1).times(power_n/2).plus(matrixOnes(ancnum-1).times(power_n/2)) ;
		Matrix Qp 		= matrixEye((dms-1)*ancnum).times(power_m);
		Matrix Qpo 		= matrixEye(ancnum-1).times(power_m);
		Matrix W1 		= Qt.plus(Qpo).inverse();
		
		 
		//compute first T1 = inv(G1'*W1*G1)*G1'*W1*h1
		Matrix T1 = (G1.transpose().times(W1).times(G1).inverse()).times(G1.transpose()).times(W1).times(h1);
		Matrix u1 = T1.getMatrix(0, dms-2, 0, 0);
		
		
		//loop 3 times
		for (int j=0; j<3; j++) {
			//init D1
			Matrix D1_A = matrixHExtend(s1.minus(u1), ancnum-1).transpose();
			Matrix D1_B = matrixHFlatten(matrixHExtend(u1,ancnum-1).transpose().minus(si.getMatrix(0, dms-2, 1, ancnum-1).transpose()));
			Matrix D1 	= matrixHConcate(D1_A, matrixSemiHDiag(D1_B, dms-1)).times(2);	   
			
			//update ri based on values from T1
			//init B1
			ri = matrixSquare(matrixMinus(xi.transpose(), u1.get(0, 0))).plus(matrixSquare(matrixMinus(yi.transpose(), u1.get(1, 0))));
			if (dimension == Dimension.THREED) {
				ri = ri.plus(matrixSquare(matrixMinus(zi.transpose(), u1.get(2, 0))));
			}
			ri = matrixSqrt(ri);
			Matrix B1 = matrixDiag(ri.getMatrix(1, ancnum-1, 0, 0)).times(2);
			
			//update W1=inv(B1QtB1' + D1QpD1')
			W1 = (B1.times(Qt).times(B1.transpose()).plus(D1.times(Qp).times(D1.transpose()))).inverse();
			
			//update T1 = inv(G1'W1G1)G1'W1h1 and u1
			T1 = (G1.transpose().times(W1).times(G1).inverse()).times(G1.transpose()).times(W1).times(h1);
			u1 = T1.getMatrix(0, dms-2, 0, 0);
		}
		//end loop 3 times
		
		
		//update ri1 values in G1
		ri1 = ri.getMatrix(1, ancnum-1, 0, 0).minus(new Matrix (ancnum-1, 1, ri.get(0, 0)));
		G1.setMatrix(0, ancnum-2, dms-1, dms-1, ri1.times(-2));
		
		
		//init h2 used values from T1
		Matrix h2 = matrixSquare(T1.minus(matrixVConcate(s1, 0)));
		
		
		//init D2
		Matrix D2 = new Matrix (dms, (dms-1)*ancnum);
		D2.setMatrix(dms-1, dms-1, 0, dms-2, u1.minus(s1).transpose().times(2));

		
		//init B2
		Matrix B2 = matrixDiag(T1.minus(matrixVConcate(s1, 0)).times(2));
		
		
		//init W2 = inv(B2 inv(G1'W1G1) B2' + D2QpD2')
		Matrix covT1 = (G1.transpose().times(W1).times(G1)).inverse();
		Matrix Bterm = B2.times(covT1).times(B2.transpose());
		Matrix Dterm = D2.times(Qp).times(D2.transpose());
		Matrix W2 = (Bterm.plus(Dterm)).inverse();
		
		
		//init T2 and u2
		Matrix T2 = (G2.transpose().times(W2).times(G2)).inverse().times(G2.transpose()).times(W2).times(h2);
		//must check if any element in T2 is a negative value. 
		if ( (T2.get(0, 0) < 0 ) || (T2.get(1, 0) < 0) || (dimension == Dimension.THREED && T2.get(2, 0) < 0) ){
			return null;
		}
		Matrix u2 = matrixSqrt(T2).arrayTimes(matrixSign(T1.getMatrix(0, dms-2, 0, 0).minus(s1))).plus(s1);
		
		//update ri and then G1 (only ri in G1)
		ri = matrixSquare(matrixMinus(xi.transpose(), u2.get(0, 0))).plus(matrixSquare(matrixMinus(yi.transpose(), u2.get(1, 0))));
		if (dimension == Dimension.THREED) {
			ri = ri.plus(matrixSquare(matrixMinus(zi.transpose(), u2.get(2, 0))));
		}
		ri = matrixSqrt(ri);
		ri1 = matrixMinus(ri.getMatrix(1, ancnum-1, 0, 0), ri.get(0, 0));
		G1.setMatrix(0, ancnum-2, dms-1, dms-1, ri1.times(-2));
		
		
		//update B2, D2, W2, h2 and then T2 and u2
		B2 = matrixDiag(matrixVConcate(u2.minus(s1), ri.get(0, 0))).times(2);
		D2.setMatrix(dms-1, dms-1, 0, dms-2, u2.minus(s1).transpose().times(2));
		covT1 = (G1.transpose().times(W1).times(G1)).inverse();
		Bterm = B2.times(covT1).times(B2.transpose());
		Dterm = D2.times(Qp).times(D2.transpose());
		W2 = (Bterm.plus(Dterm)).inverse();
		h2 = matrixSquare(matrixVConcate(u2.minus(s1), ri.get(0, 0)));
		T2 = (G2.transpose().times(W2).times(G2)).inverse().times(G2.transpose()).times(W2).times(h2);
		//must check if any element in T2 is a negative value. 
		if ( (T2.get(0, 0) < 0 ) || (T2.get(1, 0) < 0) || (dimension == Dimension.THREED && T2.get(2, 0) < 0) ){
			return null;
		}
		u2 = matrixSqrt(T2).arrayTimes(matrixSign(T1.getMatrix(0, dms-2, 0, 0).minus(s1))).plus(s1);
		
		
		Triplet u = new Triplet(u2.get(0, 0), u2.get(1, 0));
		if (dimension == Dimension.THREED) {
			u.setValue("z", u2.get(2, 0));
		}
		return u;
	}
	
	
	
	static Triplet computeWikiEstimate (Hashtable<String, Double> rangediffsubtable, List<Triplet> anclist, int index1, int index2, Dimension dimension) throws Exception {
		Triplet t1 = anclist.get(index1);
		Triplet t2 = anclist.get(index2);
		double x1 = t1.getX();
		double x2 = t2.getX();
		double y1 = t1.getY();
		double y2 = t2.getY();
		double z1 = t1.getZ();
		double z2 = t2.getZ();
		double sumsquared1 = t1.sumOfSquared(dimension);
		double sumsquared2 = t2.sumOfSquared(dimension);
		double rangediff21 = rangediffsubtable.get(index2+"_"+index1);
		Matrix A = new Matrix(anclist.size()-2, dimension == Dimension.TWOD ? 2 : 3);
		Matrix B = new Matrix(anclist.size()-2, 1);
		
		int rowid = 0;
		for (int i=0; i<anclist.size(); i++) {
			if (i != index1  &&  i != index2) {
				Triplet ti = anclist.get(i);
				double 	xi = ti.getX();
				double 	yi = ti.getY();
				double	zi = ti.getZ();
				double sumsquaredi = ti.sumOfSquared(dimension);
				double rangediffi1 = rangediffsubtable.get(i+"_"+index1);
				double 	a = (2*(xi-x1)/rangediffi1) - (2*(x2-x1)/rangediff21);
				double 	b = (2*(yi-y1)/rangediffi1) - (2*(y2-y1)/rangediff21);
				double 	c = (2*(zi-z1)/rangediffi1) - (2*(z2-z1)/rangediff21);
				double 	d = rangediff21 - rangediffi1 + ((sumsquared1-sumsquared2)/rangediff21) - ((sumsquared1-sumsquaredi)/rangediffi1);
				A.set(rowid, 0, a);
				A.set(rowid, 1, b);
				if (dimension == Dimension.THREED) {
					A.set(rowid, 2, c);
				}
				B.set(rowid, 0, d);
				rowid++;
			}
		}
		
		Matrix X = A.solve(B);
		
		Triplet x = null;
		if (X != null) {
			x = (dimension == Dimension.TWOD) ? new Triplet (X.get(0, 0), X.get(1, 0)) : new Triplet (X.get(0, 0), X.get(1, 0), X.get(2, 0));
		}
		
		return x;
	}
	
	
	
	
	private static Matrix matrixEye (int n) {
		Matrix M = new Matrix (n,n);
		for (int i=0; i<n; i++) {
			M.set(i, i, 1.0);
		}
		
		return M;
	}
	
	
	private static Matrix matrixOnes (int n) {
		return new Matrix (n, n, 1.0);
	}
	
	
	private static Matrix matrixOnes (int m, int n) {
		return new Matrix (m, n, 1.0);
	}

	
	private static Matrix matrixDiag (double[] values) {
		int size = values.length;
		Matrix A = new Matrix (size, size);
		
		for (int i=0; i<size; i++) {
			A.set(i, i, values[i]);
		}
		
		return A;
	}
	
	
	private static Matrix matrixDiag (Matrix A) throws Exception {
		Matrix B = null;
		
		if (A.getRowDimension() != 1 && A.getColumnDimension() != 1) {
			throw new Exception ("diag(): the method requires a vector input.");
		}
		
		boolean flag = A.getRowDimension() == 1;
		int dms =  flag ? A.getColumnDimension() : A.getRowDimension();
		B = new Matrix (dms, dms);
		for (int i=0; i<dms; i++) {
			B.set(i, i, flag ? A.get(0, i) : A.get(i, 0));
		}
		
		return B;
	}
	
	
	private static Matrix matrixDiagOf (Matrix A) throws Exception {
		if (A.getRowDimension() != A.getColumnDimension()) {
			throw new Exception ("diagOf(): the method requires square matrix input.");
		}
		
		int row = A.getRowDimension();
		Matrix B = new Matrix (row, 1);
		for (int i=0; i<row; i++) {
			B.set(i, 0, A.get(i, i));
		}
		
		return B;
	}
	
	
	private static Matrix matrixHConcate (Matrix A, double s) throws Exception {
		int rowA = A.getRowDimension();
		Matrix B = new Matrix (rowA, 1, s);
		return matrixHConcate(A, B);
	}
	
	
	private static Matrix matrixHConcate (Matrix A, Matrix B) throws Exception {
		int rowA = A.getRowDimension(), rowB = B.getRowDimension();
		if (rowA != rowB) {
			throw new Exception("horizontalConcate(): row dimensions mismatch: row(A) = " + rowA + "; row(B) = " + rowB);
		} 
		
		int colA = A.getColumnDimension(), colB = B.getColumnDimension();
		Matrix C = new Matrix(rowA, colA+colB);
		C.setMatrix(0, rowA-1, 0, colA-1, A);
		C.setMatrix(0, rowA-1, colA, colA+colB-1, B);
		return C;
	}
	
		
	private static Matrix matrixHConcate (Matrix[] A) throws Exception {
		Matrix C = null;
		if (A.length > 2) {
			int rowA = A[0].getRowDimension(), rowB = A[1].getRowDimension();
			if (rowA != rowB) {
				throw new Exception("horizontalConcate(): row dimensions mismatch: row(A) = " + rowA + "; row(B) = " + rowB);
			} 
			Matrix B = matrixHConcate (Arrays.copyOfRange(A, 1, A.length));
			int colA = A[0].getColumnDimension();
			int colB = B.getColumnDimension();
			C = new Matrix(rowA, colA+colB);
			C.setMatrix(0, rowA-1, 0, colA-1, A[0]);
			C.setMatrix(0, rowA-1, colA, colA+colB-1, B);
		} 
		else if (A.length == 2) {
			int rowA = A[0].getRowDimension(), rowB = A[1].getRowDimension();
			if (rowA != rowB) {
				throw new Exception("horizontalConcate(): row dimensions mismatch: row(A) = " + rowA + "; row(B) = " + rowB);
			} 
			
			int colA = A[0].getColumnDimension(), colB = A[1].getColumnDimension();
			C = new Matrix(rowA, colA+colB);
			C.setMatrix(0, rowA-1, 0, colA-1, A[0]);
			C.setMatrix(0, rowA-1, colA, colA+colB-1, A[1]);
		}
		else if (A.length == 1) {
			C = new Matrix(A[0].getRowDimension(), A[0].getColumnDimension());
			C.setMatrix(0, A[0].getRowDimension()-1, 0, A[0].getColumnDimension()-1, A[0]);
		}
		else {
			throw new Exception("horizontalConcate(): no input parameter.");
		}
		
		return C;
	}
		

	private static Matrix matrixHExtend (Matrix A, int repeat) throws Exception {
		Matrix B;
		
		if (repeat >= 2) {
			B = matrixHConcate(A, matrixHExtend(A, repeat-1));
		} else if (repeat == 1) {
			return A;
		} else {
			throw new Exception ("matrixHExtend(): @repeat input value must be positive integer.");
		}
		return B;
	}
		
	
	private static Matrix matrixHFlatten (Matrix A) {
		int colA = A.getColumnDimension();
		int rowA = A.getRowDimension();
		Matrix B = new Matrix (1, colA*rowA);
		
		int colidB=0;
		for (int r=0; r<rowA; r++) {
			B.setMatrix(0, 0, colidB, colidB+colA-1, A.getMatrix(r, r, 0, colA-1));
			colidB += colA;
		}
		
		return B;
	}
	
	
	private static Matrix matrixMinus (Matrix A, double s) {
		int row = A.getRowDimension();
		int col = A.getColumnDimension();
		Matrix B = new Matrix(row,col);
		
		for (int r=0; r<row; r++) {
			for (int c=0; c<col; c++) {
				B.set(r, c, A.get(r, c)-s);
			}
		}
		
		return B;
	}
	
	
	private static Matrix matrixSign (Matrix A) {
		int row = A.getRowDimension();
		int col = A.getColumnDimension();
		Matrix B = new Matrix(row,col);
		
		for (int r=0; r<row; r++) {
			for (int c=0; c<col; c++) {
				double val = A.get(r, c);
				B.set(r, c,  val > 0 ? 1 : (val == 0 ? 0 : (val < 0 ? -1 : val)) );
			}
		}
		
		return B;
	}
	
	
	private static Matrix matrixSemiHDiag (Matrix A, int member) throws Exception {
		int colA = A.getColumnDimension();
		int rowA = A.getRowDimension();
		if (rowA != 1 || colA % member != 0) {
			throw new Exception ("matrixSemiHDiag(): input matrix is not a row vector or mismatch number of matrix's column and group members.");
		}
		
		int rowB = colA / member;
		Matrix B = new Matrix (rowB, colA);
		for (int rowidB=0, colid=0; rowidB < rowB; rowidB++) {
			B.setMatrix(rowidB, rowidB, colid, colid+member-1, A.getMatrix(0, 0, colid, colid+member-1));
			colid += member;
		}
		
		return B;
	}
	
	
	private static Matrix matrixSquare (Matrix A) {
		int row = A.getRowDimension();
		int col = A.getColumnDimension();
		Matrix B = new Matrix(row,col);
		
		for (int r=0; r<row; r++) {
			for (int c=0; c<col; c++) {
				B.set(r, c, Math.pow(A.get(r, c), 2));
			}
		}
		
		return B;
	}
	
	
	private static Matrix matrixSqrt (Matrix A) {
		int row = A.getRowDimension();
		int col = A.getColumnDimension();
		Matrix B = new Matrix(row,col);
		
		for (int r=0; r<row; r++) {
			for (int c=0; c<col; c++) {
				B.set(r, c, Math.sqrt(A.get(r, c)));
			}
		}
				
		return B;
	}
	
	
	private static Matrix matrixVConcate (Matrix A, double s) throws Exception {
		int colA = A.getColumnDimension();
		Matrix B = new Matrix (1, colA, s);
		return matrixVConcate(A, B);
	}
	
	
	private static Matrix matrixVConcate (Matrix A, Matrix B) throws Exception {
		int colA = A.getColumnDimension(), colB = B.getColumnDimension();
		if (colA != colB) {
			throw new Exception("verticalConcate(): column dimensions mismatch: colA) = " + colA + "; col(B) = " + colB);
		} 
		
		int rowA = A.getRowDimension(), rowB = B.getRowDimension();
		Matrix C = new Matrix(rowA+rowB, colA);
		C.setMatrix(0, rowA-1, 0, colA-1, A);
		C.setMatrix(rowA, rowA+rowB-1, 0, colA-1, B);
		return C;
	}	
	
	
	private static Matrix matrixVExtend (Matrix A, int repeat) throws Exception {
		Matrix B;
		
		if (repeat >= 2) {
			B = matrixVConcate(A, matrixVExtend(A, repeat-1));
		} else if (repeat == 1) {
			return A;
		} else {
			throw new Exception ("matrixHExtend(): @repeat input value must be positive integer.");
		}
		return B;
	}
	

}