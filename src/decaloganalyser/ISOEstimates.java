package decaloganalyser;

import java.util.ArrayList;

import myUtils.Triplet;

class ISOEstimates {
	Triplet est0, est1;
	int		n;
	
	ISOEstimates (Triplet t0, Triplet t1) {
		est0 = t0;
		est1 = t1;
		n = 2;
	}
	
	ISOEstimates (Triplet t) {
		est0 = t;
		n = 1;
	}
	
	ISOEstimates () {
		n = 0;
	}
	
	void addEstimate (Triplet t) {
		if (n==0) {
			est0 = t;
			n = 1;
		}
		else if (n==1){
			est1 = t;
			n = 2;
		}
	}
	
	int getNumber () {
		return n;
	}
		
	ArrayList<Triplet> getTriplets () {
		ArrayList<Triplet> at = new ArrayList<Triplet>();
		if (n>=1) {
			at.add(est0);
		}
		if (n>=2) {
			at.add(est1);
		}
		return at;
	}
	
	Triplet getTriplet(int estindex) {	
		//i is index number starting from 0
		Triplet est;
		switch (estindex) {
		case 0: 
			est = est0;
			break;
		case 1:
			est = est1;
			break;
		default:
			est = new Triplet();
		}
		
		return est;
	}
}
